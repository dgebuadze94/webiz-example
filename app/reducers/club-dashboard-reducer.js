import {
  CLUB_DASHBOARD_REQUEST,
  CLUB_DASHBOARD_ERROR,
  CLUB_DASHBOARD_SUCCESS,
  CLUB_SCROLL_TO_TOP,
  REFRESH_CLUB
} from '../constants/action-types';

const initialState = {
  data: {},
  shouldRefresh: false,
  scrollToTop: {
    status: false,
    timeStamp: Date.now()
  },
  isLoading: false,
  error: false
};

const clubDashboardReducer = (state = initialState, action) => {
  switch (action.type) {
    case CLUB_DASHBOARD_REQUEST:
      return {
        ...state,
        isLoading: true
      };
    case CLUB_DASHBOARD_SUCCESS:
      return {
        data: action.payload,
        isLoading: false,
        error: false
      };
    case CLUB_DASHBOARD_ERROR:
      return {
        ...state,
        isLoading: false,
        error: true
      };
    case REFRESH_CLUB: {
      return {
        ...state,
        shouldRefresh: true
      };
    }
    case CLUB_SCROLL_TO_TOP: {
      return {
        ...state,
        scrollToTop: {
          status: true,
          timeStamp: action.payload.timeStamp
        }
      };
    }
    default: {
      return state;
    }
  }
};

export default clubDashboardReducer;
