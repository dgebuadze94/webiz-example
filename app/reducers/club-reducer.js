// @flow

import {
  DATA_SUCCESS,
  CLUB_DATA_ERROR,
  CLUB_DATA_REQUEST,
  CLUB_DATA_SUCCESS,
  CLUB_DATA_LOCAL,
  SEEN_PENDING
} from '../constants/action-types';

const initialState = {
  responseData: {},
  clubData: {},
  seenPending: false,
  isLoading: false,
  error: false
};

const clubReducer = (state = initialState, action) => {
  switch (action.type) {
    case DATA_SUCCESS: {
      return {
        isLoading: false,
        error: false,
        seenPending: state.seenPending,
        clubData: state.clubData,
        responseData: action.payload.responseData
      };
    }
    case SEEN_PENDING: {
      return {
        isLoading: false,
        error: false,
        seenPending: action.payload.seenPending,
        clubData: state.clubData,
        responseData: state.responseData
      };
    }
    case CLUB_DATA_SUCCESS: {
      return {
        isLoading: false,
        error: false,
        seenPending: state.seenPending,
        clubData: action.payload.clubData,
        responseData: {}
      };
    }
    case CLUB_DATA_REQUEST: {
      return {
        isLoading: true,
        error: false,
        seenPending: state.seenPending,
        clubData: {},
        responseData: {}
      };
    }
    case CLUB_DATA_ERROR: {
      return {
        ...state,
        isLoading: false,
        seenPending: state.seenPending,
        error: true
      };
    }
    case CLUB_DATA_LOCAL: {
      return {
        isLoading: false,
        error: false,
        seenPending: state.seenPending,
        clubData: action.payload.clubData,
        responseData: {}
      };
    }

    default: {
      return state;
    }
  }
};

export default clubReducer;
