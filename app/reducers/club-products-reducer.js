import {
  CLUB_PRODUCTS_REQUEST,
  CLUB_PRODUCTS_ERROR,
  CLUB_PRODUCTS_SUCCESS,
  CLUB_PRODUCTS_LOCAL,
  CLUB_SCROLL_TO_TOP,
  REFRESH_CLUB
} from '../constants/action-types';

const initialState = {
  products: {},
  shouldRefresh: false,
  scrollToTop: {
    status: false,
    timeStamp: Date.now()
  },
  isLoading: false,
  error: false
};

const clubProductsReducer = (state = initialState, action) => {
  switch (action.type) {
    case CLUB_PRODUCTS_REQUEST:
      return {
        ...state,
        isLoading: true
      };
    case CLUB_PRODUCTS_SUCCESS:
      // case CLUB_PRODUCTS_LOCAL:
      return {
        products: action.payload.products,
        isLoading: false,
        error: false
      };
    case CLUB_PRODUCTS_ERROR:
      return {
        ...state,
        isLoading: false,
        error: true
      };
    case REFRESH_CLUB: {
      return {
        ...state,
        shouldRefresh: true
      };
    }
    case CLUB_SCROLL_TO_TOP: {
      return {
        ...state,
        scrollToTop: {
          status: true,
          timeStamp: action.payload.timeStamp
        }
      };
    }
    default: {
      return state;
    }
  }
};

export default clubProductsReducer;
