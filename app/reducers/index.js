// @flow
import { combineReducers } from 'redux';
import clubReducer from './club-reducer';
import generateOtpReducer from './generate-otp-reducer';
import verifyOtpReducer from './verify-otp-reducer';
import registerReducer from './register-reducer';
import accountReducer from './account-reducer';
import allReducer from './all-reducer';
import personalDetailsReducer from './personal-details-reducer';
import shippingAddressReducer from './shipping-address-reducer';
import availableLanguagesReducer from './available-languages-reducer';
import setLanguageReducer from './set-language-reducer';
import languageDataReducer from './language-data-reducer';
import logoutReducer from './logout-reducer';
import cardsReducer from './cards-reducer';
import getIpReducer from './ip-reducer';
import ordersReducer from './orders-reducer';
import pendingOrdersReducer from './pending-orders-reducer';
import orderDetailsReducer from './order-details-reducer';
import orderHistoryReducer from './order-history-reducer';
import cancelOrderReducer from './cancel-order-reducer';
import shippingAddressListReducer from './shipping-adrress-list-reducer';
import makePaymentReducer from './make-payment-reducer';
import securitySettingsReducer from './security-settings-reducer';
import memberLoginReducer from './member-login-reducer';
import clubProductsReducer from './club-products-reducer';
import clubCategoriesReducer from './club-categories-reducer';
import categoryProductsReducer from './category-products-reducer';
import productDetailsReducer from './product-details-reducer';
import newProductsReducer from './new-products-reducer';
import makeOrderReducer from './make-order-reducer';
import productOptionsReducer from './product-options-reducer';
import updateNoticeReducer from './update-notice-reducer';
import appsflyerReducer from './appsflyer-reducer';
import availableCurrenciesReducer from './available-currencies-reducer';
import setCurrencyReducer from './set-currency-reducer';
import clubBannersReducer from './club-banners-reducer';
import clubHomeCategoriesReducer from './club-home-categories-reducer copy';
import clubMerchantDataReducer from './club-merchant-data-reducer';
import clubDashboardReducer from './club-dashboard-reducer';

// Root Reducer
const rootReducer = combineReducers({
  club: clubReducer,
  clubProducts: clubProductsReducer,
  clubDashboard: clubDashboardReducer
});

export default rootReducer;
