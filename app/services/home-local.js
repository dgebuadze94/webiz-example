import { AsyncStorage } from 'react-native';

export const setServerIP = async serverIP => {
  await AsyncStorage.setItem('serverIP', serverIP);
};

export const setServerIPLocked = async locked => {
  await AsyncStorage.setItem('serverIPLocked', JSON.stringify(locked));
};

export const saveLanguageJson = async (langId, languageData) => {
  await AsyncStorage.setItem(langId, JSON.stringify(languageData));
};

export const getLanguage = async langId => {
  const languageData = await AsyncStorage.getItem(langId);
  return JSON.parse(languageData);
};

export const localCardDataStore = async cardData => {
  await AsyncStorage.setItem('cardData', JSON.stringify(cardData));
};
