import config, { GET_ROOT, SET_ROOT, DEFAULT_IP, BACKUP_IP, GE_IP } from '../lib/config';
import Auth from '../constants/Auth';

const _fetch = (url, body = {}, token = Auth.getToken(), n = 1) => {
// deleted for security purposes
};

export const fetchClubProducts = data => _fetch(config.GET_CLUB_PRODUCTS_URL, data);

export const fetchClubDashboard = data => _fetch(config.GET_CLUB_DASHBOARD_URL, data);
