import { connect } from 'react-redux';
import ProductComponent from '../../components/club/product';
import { getProductDetails, getClubProducts, makeOrder } from '../../actions/fetch-data/fetch-data';
import {
  changeProductOptionColor,
  changeProductOptionSize,
  resetProductOptions
} from '../../actions/fetch-data/fetch-data-success';

const mapStateToProps = state => {
  return {
    selectedColor: state.selectedProductOptions.color,
    selectedSize: state.selectedProductOptions.size,
    selectedQuantity: state.selectedProductOptions.quantity,
    selectedVariation: state.selectedProductOptions.variation,
    productData: state.productDetails,
    orderId: state.makeOrder
  };
};

const mapDispatchToProps = dispatch => ({
  getProductDetails: data => dispatch(getProductDetails(data)),
  makeOrder: data => dispatch(makeOrder(data)),
  changeColor: data => dispatch(changeProductOptionColor(data)),
  changeSize: data => dispatch(changeProductOptionSize(data)),
  resetOptions: () => dispatch(resetProductOptions())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductComponent);
