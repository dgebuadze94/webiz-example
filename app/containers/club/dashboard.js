// @flow

import { connect } from 'react-redux';
import DashboardComponent from '../../components/club/dashboard';
import { getClubDashboard } from '../../actions/fetch-data/fetch-data';
import { toggleNavigationModal } from '../../actions/fetch-data/fetch-data-success';

const mapStateToProps = state => {
  return {
    dashboard: state.clubDashboard
  };
};

const mapDispatchToProps = dispatch => ({
  getData: data => dispatch(getClubDashboard(data)),
  toggleNavigationModal: () => dispatch(toggleNavigationModal())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DashboardComponent);
