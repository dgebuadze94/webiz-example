// @flow

import { connect } from 'react-redux';
import AboutComponent from '../../components/support/about';
import { toggleNavigationModal } from '../../actions/fetch-data/fetch-data-success';

const mapStateToProps = state => ({ NavModal: state.requestData.NavigationModal });

const mapDispatchToProps = dispatch => ({ toggleNavigationModal: () => dispatch(toggleNavigationModal()) });

export default connect(mapStateToProps, mapDispatchToProps)(AboutComponent);
