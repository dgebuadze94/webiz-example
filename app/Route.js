import {Actions} from 'react-native-router-flux';

const Route = (props) => {
    const {target, params} = props;
    Actions.replace(target, params);
    return null;
};

const {reset} = Actions;

Actions.reset = (target, params) => {
    const props = {target};
    if (params) {
        props.params = params;
    }
    reset('root', props);
};

export default Route;