import React, { Component } from 'react';
import { StyleSheet, View, Dimensions, TouchableOpacity, Image, Text, I18nManager } from 'react-native';
import backArrowImage from '../../../images/new/backNew.png';
import backArrowImageReverse from '../../../images/new/backNew-reverse.png';
import plusImage from '../../../images/icon/plus-icon.png';

import DeviceInfo from 'react-native-device-info';

const DEVICE_HEIGHT = Dimensions.get('window').height;
const SMALL_SCREEN = DEVICE_HEIGHT < 600;

export default class Header extends Component {
  constructor(props) {
    super(props);
  }

  renderRightView = () => {
    let { rightView, onPressRightView, rightViewDisabled, rightViewStyle } = this.props;

    if (typeof rightView === 'function') {
      return <View style={styles.rightViewContainer}>{rightView()}</View>;
    } else if (rightView) {
      return (
        <View style={styles.rightView}>
          <TouchableOpacity
            style={styles.rightLeftViewContent}
            onPress={() => onPressRightView && onPressRightView()}
            disabled={rightViewDisabled}
            underlayColor="#fff"
            accessible={false}
            accessibilityLabel="right_view"
            testID="right_view"
          >
            <Image source={rightView} style={[styles.rightViewImage, rightViewStyle]} />
          </TouchableOpacity>
        </View>
      );
    }
  };

  renderLeftView = () => {
    let {
      displayLeftAsBackButton,
      backButtonImage,
      leftView,
      onPressLeftView,
      leftViewDisabled,
      language
    } = this.props;
    if (displayLeftAsBackButton) {
      const image = backButtonImage ? backButtonImage : backArrowImage;
      return (
        <View style={styles.leftView}>
          <TouchableOpacity
            style={styles.rightLeftViewContent}
            onPress={() => onPressLeftView && onPressLeftView()}
            disabled={leftViewDisabled}
            underlayColor="#fff"
            accessible={false}
            accessibilityLabel="left_btn"
            testID="left_btn"
          >
            <Image source={image} style={styles.leftViewImage} />
          </TouchableOpacity>
        </View>
      );
    } else if (typeof leftView === 'function') {
      return <View style={styles.leftViewContainer}>{leftView()}</View>;
    }
  };

  renderTitle = () => {
    let { title, titleStyle } = this.props;
    return <Text style={[styles.title, titleStyle]}>{title ? title : ''}</Text>;
  };

  render() {
    let {
      shouldDisplayUnderline,
      underlineWidth,
      underlineColor,
      backgroundColor,
      height,
      importantStyle
    } = this.props;
    let underLine = shouldDisplayUnderline ? (underlineWidth ? underlineWidth : 1) : 0;
    const hasNotch = DeviceInfo.hasNotch();
    let formatedHeight = hasNotch ? (height ? height - 25 : 45) : height ? height : 70;
    return (
      <View
        style={[
          styles.container,
          {
            justifyContent: hasNotch ? 'flex-start' : 'center',
            paddingTop: hasNotch ? 5 : 0,
            height: formatedHeight,
            backgroundColor: backgroundColor ? backgroundColor : '#ffffff',
            borderColor: underlineColor ? underlineColor : '#EAEAEA',
            borderBottomWidth: underLine
          },
          importantStyle
        ]}
      >
        {this.renderLeftView()}
        {this.renderTitle()}
        {this.renderRightView()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: SMALL_SCREEN ? 50 : 70,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'center'
  },
  rightViewContainer: {
    flex: 1,
    justifyContent: 'center',
    position: 'absolute',
    alignSelf: 'flex-end',
    height: '100%'
  },
  rightView: {
    width: '15%',
    height: '100%',
    justifyContent: 'flex-start',
    position: 'absolute',
    alignSelf: 'flex-end'
  },
  rightLeftViewContent: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  rightViewImage: {
    height: 14.5,
    width: 14.5,
    resizeMode: 'contain'
  },
  leftViewContainer: {
    flex: 1,
    justifyContent: 'center',
    position: 'absolute',
    alignSelf: 'flex-start',
    height: '100%'
  },
  leftView: {
    width: '15%',
    height: '100%',
    paddingBottom: 5,
    justifyContent: 'flex-start',
    position: 'absolute',
    alignSelf: 'flex-start'
  },
  leftViewImage: {
    height: SMALL_SCREEN ? 14 : 18,
    width: SMALL_SCREEN ? 14 : 18,
    resizeMode: 'contain',
    transform: I18nManager.isRTL ? [{ rotate: '180deg' }] : [{ rotate: '0deg' }]
  },
  titleContainer: {
    flex: 1,
    justifyContent: 'center',
    position: 'absolute'
  },
  title: {
    fontSize: SMALL_SCREEN ? 13 : 15,
    color: 'rgb(50,44,59)',
    fontFamily: 'OpenSans-Semibold'
  }
});
