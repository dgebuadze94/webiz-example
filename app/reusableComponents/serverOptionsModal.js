import React, { Component } from 'react';
import { View, TouchableOpacity, Text, TextInput, Modal, StyleSheet } from 'react-native';
import { Actions, ActionConst } from 'react-native-router-flux';
import Auth from '../constants/Auth';
import { DEFAULT_IP, DEV_IP, LOCAL_IP, SET_ROOT } from '../lib/config';
import { setServerIP, setServerIPLocked } from '../services/home-local';

export default class serverOptionsModal extends Component {
  state = { customIP: '' };
  changeServerIP = serverIP => {
    Auth.removeAll();
    setServerIP(serverIP);
    serverIP === DEFAULT_IP ? setServerIPLocked(false) : setServerIPLocked(true);
    Actions.Index({ shouldRefresh: true, type: ActionConst.REPLACE });

    this.props.onDone();
  };
  render() {
    return (
      <Modal visible={this.props.visible} animationType="slide" transparent hideModalContentWhileAnimating>
        <TouchableOpacity style={styles.modalBck} onPress={() => this.setState({ modalVisible: false })}>
          <TouchableOpacity style={styles.modalContent} activeOpacity={1}>
            <TouchableOpacity onPress={() => this.changeServerIP(DEFAULT_IP)} style={styles.serverOption}>
              <Text style={{ fontSize: 16 }}>api.testAPP.net</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.changeServerIP(DEV_IP)} style={styles.serverOption}>
              <Text style={{ fontSize: 16 }}>dev.testAPP.net</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.changeServerIP(LOCAL_IP)} style={styles.serverOption}>
              <Text style={{ fontSize: 16 }}>10.20.0.250</Text>
            </TouchableOpacity>
            <View style={styles.serverOption}>
              <TextInput
                style={{ paddingLeft: 15 }}
                placeholder="custom"
                ref={value => (this.serverIP = value)}
                onChangeText={value => this.setState({ customIP: value })}
                onSubmitEditing={() => this.changeServerIP(`http://${this.state.customIP}/public/api/app/`)}
              />
            </View>
            <TouchableOpacity
              onPress={() => {
                Actions.niceToMeet({ type: ActionConst.REPLACE });
                this.props.onDone();
              }}
              style={styles.serverOption}
            >
              <Text style={{ fontSize: 16 }}>ACTUALLY LOG OUT</Text>
            </TouchableOpacity>
          </TouchableOpacity>
        </TouchableOpacity>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  modalBck: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(49, 42, 56, 0.79)'
  },
  modalContent: {
    width: 300,
    height: 400,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center'
  },
  serverOption: {
    borderBottomColor: 'rgb(245, 246, 248)',
    borderBottomWidth: 2,
    width: '100%',
    height: '20%',
    justifyContent: 'center',
    alignItems: 'center'
  }
});
