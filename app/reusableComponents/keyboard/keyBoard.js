import React, { Component } from 'react';
import { StyleSheet, View, Dimensions } from 'react-native';
import {KeyBoardKey, BlankKeyBoardKey, DeleteKey} from "./keyBoardKey";
const DEVICE_HEIGHT = Dimensions.get('window').height;
const SMALL_SCREEN = DEVICE_HEIGHT < 600;

export default class KeyBoard extends Component {

	constructor(props){
		super(props);

		this.onPressNum = this.onPressNum.bind(this);
		this.onPressDel = this.onPressDel.bind(this);
	}


	onPressNum = value => {
		let textValue = this.props.value;
		let {onValueChanged, maxLength} = this.props;

		if (!maxLength || (textValue.length < maxLength)) {
			textValue = textValue+ value.toString();
		}

		onValueChanged(textValue);
	};

	onPressDel = () => {
		let textValue = this.props.value;
		let {onValueChanged} = this.props;

		textValue = textValue.slice(0, -1);

		onValueChanged(textValue);
	};

	clear = () => {
		let {onValueChanged} = this.props;
		this.props.value = '';
		onValueChanged('');
	};

	renderKey(value) {
		const { onPressNum} = this.props;
		return (
			<KeyBoardKey value={value}
						 accessible={false}
						 accessibilityLabel={"key_"+value}
						 testID={"key_"+value}
						 onPressKey={() => this.onPressNum(value)}/>
		);
	}

	renderDeleteKey(){
		const {onPressDel} = this.props;
		return (
			<DeleteKey
				onPress={() => this.onPressDel()}
				onLongPress={() => this.clear()}
			/>
		)

			//return this.renderKey("del", false);
	}

	render() {
		return (
			<View style={styles.container}>
				<View style={styles.row}>
					{this.renderKey(1)}
					{this.renderKey(2)}
					{this.renderKey(3)}
				</View>
				<View style={styles.row}>
					{this.renderKey(4)}
					{this.renderKey(5)}
					{this.renderKey(6)}
				</View>
				<View style={styles.row}>
					{this.renderKey(7)}
					{this.renderKey(8)}
					{this.renderKey(9)}
				</View>
				<View style={styles.row}>
					<BlankKeyBoardKey/>
					{this.renderKey(0)}
					{this.renderDeleteKey()}
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		backgroundColor: '#dbd9e6',
		width: '100%',
		height: SMALL_SCREEN ? DEVICE_HEIGHT * 0.35 : 280,
		justifyContent: 'space-between',
		borderTopColor: '#dbd9e6',
		borderTopWidth: 1
	},
	row: {
		width: '100%',
		height: '25%',
		flexDirection: 'row',
		justifyContent: 'space-between',
		borderBottomColor: '#dbd9e6',
		borderBottomWidth: 1
	},

});
