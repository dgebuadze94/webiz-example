import { Image, StyleSheet, Text, View } from 'react-native';
import backSpaceImage from '../../../images/new/backSpace.png';
import React from 'react';
import Ripple from 'react-native-material-ripple';

const KeyBoardKey = ({ value, onPressKey }) => (
	<Ripple
		rippleCentered={true}
		rippleOpacity={0.1}
		rippleDuration={200}
		style={styles.cell}
		onPress={() => onPressKey(value)}
		accessible={false}
		accessibilityLabel={'key_' + value}
		testID={'key_' + value}
	>
		<Text style={styles.cellText}>{value}</Text>
	</Ripple>
);

const DeleteKey = ({ onPress, onLongPress }) => (
	<Ripple
		rippleOpacity={0.1}
		rippleDuration={200}
		style={styles.cell}
		delayLongPress={800}
		onPress={() => onPress()}
		onLongPress={() => onLongPress()}
		accessible={false}
		accessibilityLabel="delete_key"
		testID="delete_key"
	>
		<Image source={backSpaceImage} style={styles.cellImage} />
	</Ripple>
);

const BlankKeyBoardKey = () => <View style={styles.cell} />;

const styles = StyleSheet.create({
	cell: {
		width: '33.33%',
		height: '100%',
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: 'rgb(252, 252, 253)',
		borderRightColor: '#dbd9e6',
		borderRightWidth: 1
	},
	cellText: {
		fontFamily: 'Gilroy-Medium',
		fontSize: 23,
		color: 'rgb(57, 49, 67)'
	},
	cellImage: {
		width: 28,
		height: 20,
		resizeMode: 'contain'
	}
});

export { KeyBoardKey, BlankKeyBoardKey, DeleteKey };
