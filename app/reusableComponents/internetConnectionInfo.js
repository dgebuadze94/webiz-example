import React, { Component } from 'react';
import { View, NetInfo, TouchableOpacity, Text, StyleSheet, SafeAreaView } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'language-provider';

export class internetConnectionInfo extends Component {
	state = {
		isConnected: true, 
		netStatusHistory: [],
		visible: false
	};
	componentWillMount() {
		NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectivityChange);
	}

	componentWillUnmount() {
		NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectivityChange);
	}

	handleConnectivityChange = isConnected => {
		const netStatusHistory = this.state.netStatusHistory.slice();
		netStatusHistory.push(isConnected);

		const index = netStatusHistory.length-1;
		index!==0 && netStatusHistory[index] && Actions.refresh({ key: this.props.name});
		
		this.setState({netStatusHistory, isConnected});

		setTimeout(()=>{
			this.setState({visible: !isConnected})
		}, 2000)		
	};

	renderNoConnection =() => {
		return (
			!this.state.isConnected ? 
			<SafeAreaView style={styles.container}>
				<TouchableOpacity style={styles.content}>
					<Text style={styles.text}>{this.props.strings.no_internet}</Text>
				</TouchableOpacity>
			</SafeAreaView> : <View/>
		)	
	}

	renderConnectionRenew =index=>{
		return index!==0 && this.state.visible ? (
			<SafeAreaView style={styles.container}>
				<TouchableOpacity style={[styles.content,styles.connectedView]} >
					<Text style={styles.text}> {this.props.strings.back_online}</Text>
				</TouchableOpacity>
			</SafeAreaView> 
		) : <View/>	 
	}

	render() {
		const { netStatusHistory } = this.state;
		const index = netStatusHistory.length-1;

		return netStatusHistory[index] ? this.renderConnectionRenew(index) :this.renderNoConnection()
	}
}

export default connect(internetConnectionInfo);

const styles = StyleSheet.create({
	container: {
		position: 'absolute',
		width: '100%',
		height: 74,
		backgroundColor: 'transparent'
	},
	connectedView: {
		backgroundColor: "rgb(0, 144, 0)"
	},
	content: {
		backgroundColor: '#ff004f',
		width: '100%',
		height: 30,
		justifyContent: 'center',
		alignItems: 'center'
	},
	text: {
		color: '#fff',
		fontWeight: 'bold',
		fontSize: 14
	}
});
