import React, { Component } from 'react';
import { View, TouchableWithoutFeedback, StyleSheet, Dimensions } from 'react-native';

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;
const SMALL_SCREEN = DEVICE_HEIGHT < 600 ? true : false;

export default class Switch extends Component {
	state = {
		value: this.props.value
	};
	toggle = () => {
		let { value } = { ...this.state };
		value = !value;
		this.setState({ value });
		this.props.toggle(value);
	};
	render() {
		const { locked } = this.props;
		return (
			<TouchableWithoutFeedback
				{...this.props}
				style={{ width: 54, height: 30 }}
				onPress={() => !locked && this.toggle()}
			>
				<View style={[styles.container, this.state.value && styles.containerActive, locked && styles.containerLocked]}>
					<View style={styles.circle} />
				</View>
			</TouchableWithoutFeedback>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		width: SMALL_SCREEN ? 49 : 54,
		height: SMALL_SCREEN ? 25 : 30,
		borderRadius: 15,
		backgroundColor: '#e5e3ee',
		alignItems: 'flex-start',
		justifyContent: 'center'
	},
	containerActive: {
		backgroundColor: 'rgb(21, 238, 195)',
		alignItems: 'flex-end'
	},
	containerLocked: {
		backgroundColor: '#e5e3ee',
		alignItems: 'flex-end'
	},
	circle: {
		width: SMALL_SCREEN ? 21 : 26,
		height: SMALL_SCREEN ? 21 : 26,
		borderRadius: SMALL_SCREEN ? 10.5 : 13,
		marginHorizontal: 2,
		backgroundColor: '#ffffff'
	}
});
