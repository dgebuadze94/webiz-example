import React, { Component } from 'react';
import { StyleSheet, View, TextInput, Dimensions, Animated, Platform, Text } from 'react-native';

const DEVICE_HEIGHT = Dimensions.get('window').height;
const SMALL_SCREEN = DEVICE_HEIGHT < 600 ? true : false;

export default class FloatingLabelInput extends Component {
  state = {
    isFocused: false
  };

  componentWillMount() {
    this._animatedIsFocused = new Animated.Value(this.props.value === '' ? 0 : 1);
  }

  componentDidUpdate() {
    Animated.timing(this._animatedIsFocused, {
      toValue: this.state.isFocused || this.props.value !== '' ? 1 : 0,
      duration: 200
    }).start();
  }

  handleFocus = () => {
    this.setState({ isFocused: true });
    if (this.props.onFocus) {
      this.props.onFocus();
    }
  };
  handleBlur = () => {
    this.setState({ isFocused: false });
    if (this.props.onBlur) {
      this.props.onBlur();
    }
  };

  focus = () => this.input.focus();
  blur = () => this.input.blur();
  update = () => this.input.update();
  clear = () => this.input.clear();
  isFocused = () => this.input.isFocused();

  render() {
    const { label, labelStyle, textInputStyle, containerStyle, required, labelDistance = 36, ...props } = this.props;
    const { isFocused } = this.state;
    const labelStyleDefault = [
      styles.label,
      {
        bottom: this._animatedIsFocused.interpolate({
          inputRange: [0, 1],
          outputRange: SMALL_SCREEN ? [0, 26] : [0, labelDistance]
        }),
        fontSize: this._animatedIsFocused.interpolate({
          inputRange: [0, 1],
          outputRange: SMALL_SCREEN ? [13, 12] : [15, 14]
        }),
        marginBottom: this._animatedIsFocused.interpolate({
          inputRange: [0, 1],
          outputRange: SMALL_SCREEN ? [5, 1] : [10, 5]
        })
      },
      Platform.OS === 'ios' && { textAlign: 'left' }
    ];

    const containerStyleDefault = [
      styles.textInputContainer,
      { borderBottomColor: isFocused ? colors.dark : colors.paleGreyTwo },
      ,
    ];

    return (
      <View>
        <View style={[containerStyleDefault, containerStyle]}>
          <Animated.Text style={[labelStyleDefault, labelStyle]}>
            {label}
          </Animated.Text>

          <TextInput
            {...props}
            style={[styles.textInput, textInputStyle]}
            onFocus={this.handleFocus}
            onBlur={this.handleBlur}
            underlineColorAndroid="transparent"
            ref={input => (this.input = input)}
          />
        </View>
      </View>
    );
  }
}
const colors = {
  dark: 'rgba(57, 49, 67, 0.59)',
  paleGreyTwo: 'rgb(219, 217, 230)'
};

const styles = StyleSheet.create({
  textInputContainer: {
    height: SMALL_SCREEN ? 43 : 64,
    borderBottomWidth: 1,
    borderBottomColor: colors.paleGreyTwo
  },
  label: {
    width: '100%',
    height: 23,
    lineHeight: 23,
    marginBottom: SMALL_SCREEN ? 5 : 10,
    fontFamily: 'OpenSans-Regular',
    textAlignVertical: 'bottom',
    position: 'absolute',
    left: 0,
    color: 'rgb(162, 159, 167)'
  },
  textInput: {
    fontSize: SMALL_SCREEN ? 13 : 15,
    lineHeight: SMALL_SCREEN ? 20 : 23,
    color: 'rgb(58, 50, 68)',
    width: '100%',
    fontFamily: 'OpenSans-Regular',
    textAlignVertical: 'bottom',
    position: 'absolute',
    bottom: 0,
    justifyContent: 'flex-end',
    textAlign: 'left'
  }
});
