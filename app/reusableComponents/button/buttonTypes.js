import React from 'react';
import { Image, StyleSheet, TouchableOpacity, View, Dimensions, I18nManager } from 'react-native';

import nextArrowDisabledImage from '../../../images/new/nextArrowDisabled.png';
import nextArrowActiveImage from '../../../images/new/nextArrowActive.png';
import addButtonImage from '../../../images/new/addButton.png';
import tickButtonImage from '../../../images/new/tickButton.png';
import loaderGif from '../../../images/new/buttonLoader.gif';

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;
const SMALL_SCREEN = DEVICE_HEIGHT < 600;

const NextButton = ({ active, onPress, style, arrowStyle }) => (
	<TouchableOpacity
		accessible={false}
		accessibilityLabel="next_btn"
		testID="next_btn"
		style={[styles.button, style]}
		onPress={() => onPress()}
	>
		<Image
			source={active ? nextArrowActiveImage : nextArrowDisabledImage}
			style={
				I18nManager.isRTL
					? [styles.buttonImage, arrowStyle, { transform: [{ rotate: '180deg' }] }]
					: [styles.buttonImage, arrowStyle]
			}
		/>
	</TouchableOpacity>
);

const AddButton = ({ active, onPress }) => (
	<TouchableOpacity
		accessible={false}
		accessibilityLabel="next_btn"
		testID="next_btn"
		style={styles.button}
		onPress={() => onPress()}
	>
		<Image source={addButtonImage} style={styles.buttonImage} />
	</TouchableOpacity>
);

const TickButton = ({ active, onPress }) => (
	<TouchableOpacity
		accessible={false}
		accessibilityLabel="next_btn"
		testID="next_btn"
		style={styles.button}
		onPress={() => onPress()}
	>
		<Image source={tickButtonImage} style={styles.buttonImage} />
	</TouchableOpacity>
);

const LoadingButton = () => (
	<View style={[styles.button, styles.loadingButton]}>
		<Image source={loaderGif} style={styles.loadingButtonImage} />
	</View>
);

const styles = StyleSheet.create({
	button: {
		height: SMALL_SCREEN ? DEVICE_HEIGHT * 0.065 : 51,
		width: SMALL_SCREEN ? DEVICE_HEIGHT * 0.085 : 68,
		justifyContent: 'center',
		alignItems: 'center',
		alignSelf: 'flex-end',
		marginTop: 10,
		marginRight: 30,
		marginBottom: SMALL_SCREEN ? 27 : 30,
		zIndex: 1000
	},
	buttonImage: {
		width: '100%',
		height: '100%',
		resizeMode: 'contain'
	},
	loadingButton: {
		borderRadius: SMALL_SCREEN ? DEVICE_HEIGHT * 0.03 : 25.5,
		backgroundColor: '#793cd9'
	},
	loadingButtonImage: {
		width: '60%',
		height: '100%',
		resizeMode: 'contain'
	}
});

export { NextButton, LoadingButton, AddButton, TickButton };
