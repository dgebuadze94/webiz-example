import React, { Component } from 'react';
import { NextButton, LoadingButton, AddButton, TickButton } from './buttonTypes';

export default class Button extends Component {
	state = {
		isLoading: false
	};

	renderNextButton() {
		const { onPress, active, style, arrowStyle } = this.props;
		return <NextButton active={active} style={style} arrowStyle={arrowStyle}
						   onPress={() => onPress()} />;
	}

	renderAddButton() {
		const { onPress, active } = this.props;
		return <AddButton active={active} onPress={() => onPress()} />;
	}

	renderTickButton() {
		const { onPress, active } = this.props;
		return <TickButton active={active} onPress={() => onPress()} />;
	}

	renderLoadingButton() {
		return <LoadingButton />;
	}

	render() {
		const { type } = this.props;
		switch (type) {
			default:
				return this.renderNextButton();
			case 'next':
				return this.renderNextButton();
			case 'loading':
				return this.renderLoadingButton();
			case 'add':
				return this.renderAddButton();
			case 'tick':
				return this.renderTickButton();
		}
	}
}
