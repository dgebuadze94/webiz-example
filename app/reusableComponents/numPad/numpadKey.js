import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, Dimensions } from 'react-native';
import Ripple from 'react-native-material-ripple';
import backSpaceImage from '../../../images/new/backSpace.png';

const DEVICE_HEIGHT = Dimensions.get('window').height;
const SMALL_SCREEN = DEVICE_HEIGHT < 600 ? true : false;

export default class numpadKey extends Component {
	state = { isActive: false };

	onPressIn = () => {
		this.setState({ isActive: true });
	};
	onPressOut = () => {
		this.setState({ isActive: false });
		this.props.onPress(this.props.value);
	};
	onLongPress = () => {
		this.props.onLongPress(this.props.value);
	};

	render() {
		const { value, isDelete } = this.props;
		return (
			<Ripple
				style={styles.ripple}
				rippleSize={SMALL_SCREEN ? 66 : 76}
				rippleCentered={true}
				rippleDuration={200}
				rippleColor={'rgb(255,255,255)'}
				onPressIn={this.onPressIn}
				onPressOut={this.onPressOut}
				onLongPress={this.onLongPress}
				delayLongPress={5000}
				activeOpacity={1}
				accessible={false}
				accessibilityLabel={`key_${value}`}
				testID={`key_${value}`}
			>
				<View style={[styles.circle, { borderWidth: this.state.isActive ? 3 : 1 }]}>
					{isDelete ? (
						<Image source={backSpaceImage} style={styles.image} />
					) : (
						<Text style={styles.value}>{value}</Text>
					)}
				</View>
			</Ripple>
		);
	}
}

const styles = StyleSheet.create({
	ripple: {
		width: SMALL_SCREEN ? 66 : 76,
		height: SMALL_SCREEN ? 66 : 76
	},
	circle: {
		width: SMALL_SCREEN ? 65 : 75,
		height: SMALL_SCREEN ? 65 : 75,
		borderRadius: SMALL_SCREEN ? 32.5 : 37.5,
		justifyContent: 'center',
		alignItems: 'center',
		borderColor: '#fff'
	},
	value: {
		textAlign: 'center',
		fontSize: SMALL_SCREEN ? 28 : 30,
		color: 'rgb(255,255,255)',
		fontFamily: 'BrandonGrotesque-Medium'
	},
	image: {
		width: 28,
		height: 20,
		resizeMode: 'contain'
	}
});
