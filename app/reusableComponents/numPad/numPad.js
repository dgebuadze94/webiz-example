import React, { Component } from 'react';
import { View, StyleSheet, Dimensions } from 'react-native';
import NumpadKey from './numpadKey';

const DEVICE_HEIGHT = Dimensions.get('window').height;
const SMALL_SCREEN = DEVICE_HEIGHT < 600 ? true : false;

export default class numPad extends Component {
	onPressKey = newValue => {
		let { value, onPressKey } = this.props;
		value = value + newValue.toString();
		onPressKey(value);
	};

	onPressDelete = () => {
		let { value, onPressKey } = this.props;
		value = value.slice(0, -1);
		onPressKey(value);
	};

	renderKey(value, isDelete) {
		return (
			<View style={styles.key}>
				<NumpadKey
					value={value}
					onPress={value => (isDelete ? this.onPressDelete() : this.onPressKey(value))}
					onLongPress={value => this.props.onLongPressKey(value)}
					isDelete={isDelete}
				/>
			</View>
		);
	}
	render() {
		return (
			<View style={styles.container}>
				<View style={styles.rowContainerFirst}>
					{this.renderKey('1')}
					{this.renderKey('2')}
					{this.renderKey('3')}
				</View>
				<View style={styles.rowContainer}>
					{this.renderKey('4')}
					{this.renderKey('5')}
					{this.renderKey('6')}
				</View>
				<View style={styles.rowContainer}>
					{this.renderKey('7')}
					{this.renderKey('8')}
					{this.renderKey('9')}
				</View>
				<View style={styles.rowContainer}>
					{this.renderKey(' ')}
					{this.renderKey('0')}
					{this.renderKey('<', true)}
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		width: '100%'
	},
	rowContainerFirst: {
		height: SMALL_SCREEN ? 65 : 75,
		width: '100%',
		flexDirection: 'row',
		alignItems: 'center'
	},
	rowContainer: {
		height: SMALL_SCREEN ? 65 : 75,
		width: '100%',
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		marginTop: SMALL_SCREEN ? 15 : 25
	},
	lastKey: {
		alignItems: 'center',
		justifyContent: 'center',
		alignSelf: 'center'
	},
	key: {
		width: '33.3%',
		justifyContent: 'center',
		alignItems: 'center'
	}
});
