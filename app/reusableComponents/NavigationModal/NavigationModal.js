import React, { Component } from 'react';
import { View, Image, Text, StyleSheet, SafeAreaView, FlatList, TouchableOpacity } from 'react-native';
import Modal from 'react-native-modal';
import connect from '../../languageProvider/connect';
import Strings from '../../localization/strings';
import AccountInActiveImage from '../../../images/tabbar/account_inactive.png';
import SupportInactiveImage from '../../../images/tabbar/support_inactive.png';
import MoreInactiveImage from '../../../images/tabbar/more_inactive.png';
import AboutImage from '../../../images/tabbar/about.png';
import nextArrow from '../../../images/club/nextArrow.png';

class NavigationModal extends Component {
  state = {
    data: [
      {
        title: Strings.my_account,
        path: 'accountDetails',
        image: AccountInActiveImage
      },
      {
        title: Strings.support_center,
        path: 'support',
        image: SupportInactiveImage
      },
      {
        title: Strings.privacy_policy,
        path: 'privacyPolicy',
        image: MoreInactiveImage
      },
      {
        title: Strings.about_testAPP,
        path: 'about',
        image: AboutImage
      }
    ]
  };

  Item = ({ item, index }) => {
    return (
      <TouchableOpacity
        style={[styles.item, index === this.state.data.length - 1 && { borderBottomWidth: 1 }]}
        onPress={() => {
          this.props.toggleNavigationModal();
        }}
      >
        <View style={{ flexDirection: 'row' }}>
          <Image source={item.image} style={styles.icon} />
          <Text style={styles.title}>{item.title}</Text>
        </View>
        <Image source={nextArrow} style={styles.nextArrow} />
      </TouchableOpacity>
    );
  };

  render() {
    const { visible, toggleNavigationModal, ...props } = this.props;
    const { data } = this.state;

    return (
      <Modal
        isVisible={visible}
        style={{ margin: 0, padding: 0 }}
        {...props}
        swipeDirection="down"
        onSwipeComplete={() => toggleNavigationModal()}
        onBackButtonPress={() => toggleNavigationModal()}
        onBackdropPress={() => toggleNavigationModal()}
        animationIn="slideInUp"
        animationOut="slideOutDown"
        useNativeDriver
      >
        <View style={styles.content}>
          <View style={styles.slideLineContainer}>
            <View style={styles.slideLine}></View>
          </View>
          <FlatList data={data} renderItem={this.Item} />
        </View>
      </Modal>
    );
  }
}
export default connect(NavigationModal);

const styles = StyleSheet.create({
  content: {
    position: 'absolute',
    left: 0,
    bottom: 0,
    width: '100%',
    backgroundColor: '#fff',
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    paddingTop: 30,
    paddingBottom: 70
  },
  item: {
    height: 53,
    borderTopWidth: 1,
    paddingHorizontal: 20,
    borderColor: 'rgb(229,229,229)',
    flexDirection: 'row',
    // marginBottom: 20,
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  title: {
    marginLeft: 15,
    fontSize: 15,
    color: 'rgb(58,50,68)'
  },
  icon: {
    width: 25,
    height: 19,
    resizeMode: 'contain'
  },
  slideLineContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: 20
  },
  slideLine: {
    width: 37,
    height: 4,
    backgroundColor: '#dcdee3',
    margin: 'auto',
    borderRadius: 5
  },
  nextArrow: {
    width: 16,
    height: 12,
    resizeMode: 'contain'
  }
});
