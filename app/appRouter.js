import React from 'react';
import { Scene, Router, ActionConst, Modal, Stack, Overlay } from 'react-native-router-flux';
import { StyleSheet, Text, Image, View, Animated, Platform } from 'react-native';
import AboutContainer from './containers/support/about';
import internetConnectionInfo from './reusableComponents/internetConnectionInfo';
import { StackViewStyleInterpolator } from 'react-navigation-stack';
import Strings from './localization/strings';
import ClubDashboardContainer from './containers/club/dashboard';
import ProductContainer from './containers/club/product';
import { clubScrollToTop } from './actions/fetch-data/fetch-data-success';
import { store } from './app';

const AppRouter = focused => {
  const SimpleLineIcon = ({ title, focused }) => {
    var imageSource = { ClubActiveImage };

    switch (title) {
      case 'my_orders':
        imageSource = focused ? OrderActiveImage : OrderInactiveImage;
        break;
      case 'club':
        imageSource = focused ? ClubActiveImage : ClubInactiveImage;
        break;
    }

    return (
      <View style={[styles.absoluteContainer]}>
        <View style={styles.container}>
          <View style={styles.imageContainer}>
            <Image source={imageSource} style={styles.image} />
            <Text style={styles.tabBarLabel}>{Strings[title].toUpperCase()}</Text>
          </View>
        </View>
      </View>
    );
  };

  const styles = StyleSheet.create({
    absoluteContainer: {
      position: 'absolute',
      width: '100%',
      height: 70,
      top: -4,
      justifyContent: 'center',
      alignItems: 'center',
      borderColor: 'rgb(229, 229, 229)',
      borderBottomWidth: 1,
      borderRightWidth: 1
    },
    container: {
      width: '100%',
      height: '100%',
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'row'
    },
    imageContainer: {
      width: '100%',
      height: '100%',
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center'
    },
    image: {
      width: 28,
      height: 25,
      resizeMode: 'contain'
    },
    tabBarLabel: {
      color: 'rgba(50,44,59,1)',
      fontSize: 12,
      fontFamily: 'Gilroy-SemiBold',
      marginLeft: 9,
      paddingTop: 5
    }
  });

  const transitionConfig = () => ({
    screenInterpolator: StackViewStyleInterpolator.forFade
  });

  return (
    <Router uriPrefix={'www.example.com'}>
      <Overlay key="overlay">
        <Modal key="modal" hideNavBar transitionConfig={transitionConfig}>
          <Stack key="root">
            <Scene
              key="tabbar"
              tabs={true}
              initialTabIndex={0}
              tabBarPosition={'bottom'}
              initial={true}
              hideNavBar
              showLabel={false}
              tabBarStyle={{
                height: 72,
                flexDirection: 'row',
                justifyContent: 'center',
                borderTopWidth: 0.5,
                borderBottomWidth: 1,
                borderColor: 'rgb(229, 229, 229)',
                backgroundColor: '#FFFFFF',
                paddingBottom: 5,
                paddingTop: 5
              }}
              type={ActionConst.REPLACE}
              activeTintColor="#7326D0"
              inactiveTintColor="#322C3B"
              tabBarOnPress={(scene, jumpToIndex) => {
                const { navigation, defaultHandler } = scene;
                if (navigation.state.routeName === 'clubDetails' && navigation.isFocused()) {
                  store.dispatch(clubScrollToTop());
                }

                defaultHandler();
              }}
            >
              <Scene
                key="order"
                title="my_orders"
                hideNavBar
                icon={SimpleLineIcon}
                type={ActionConst.REPLACE}
                showLabel={false}
                accessible={false}
                accessibilityLabel="order_btn"
                testID="order_btn"
              >
                <Scene key="orderList" title="Order List" component={OrderContainer} />
                <Scene key="orderInside" title="Order Inside" hideTabBar component={OrderInsideContainer} />
              </Scene>
              <Scene
                key="clubDetails"
                title="club"
                hideNavBar
                icon={SimpleLineIcon}
                tabBarLabel={'CLUBTE'}
                accessible={false}
                accessibilityLabel="club_btn"
                testID="club_btn"
                transitionConfig={() => ({ screenInterpolator: StackViewStyleInterpolator.forHorizontal })}
              >
                <Scene key="club" title="Club" component={ClubDashboardContainer} />
                <Scene key="product" title="Product" hideTabBar component={ProductContainer} />
              </Scene>
            </Scene>
            <Scene key="about" title="About" hideTabBar hideNavBar component={AboutContainer} />
            <Scene key="privacyPolicy" title="Teams" hideTabBar hideNavBar component={PrivacyPolicyContainer} />
          </Stack>
        </Modal>
        <Scene key="internetConnectionInfo" component={internetConnectionInfo} />
        <Scene key="updateNotice" component={UpdateNoticeContainer} />
      </Overlay>
    </Router>
  );
};

export default AppRouter;
