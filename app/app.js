import React from 'react';

import { Text, View, StatusBar } from 'react-native';

import { Provider, connect } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import { PersistGate } from 'redux-persist/integration/react';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import thunk from 'redux-thunk';
import rootReducer from './reducers';
import Strings from './localization/strings';
import { LanguageProvider } from 'language-provider';
import DropdownAlert from 'react-native-dropdownalert';
import AsyncStorage from '@react-native-community/async-storage';

// tabbar images
import OrderNotificationIcon from '../images/order_notification_icon.png';
import { I18nManager } from 'react-native';
import AppRouter from './appRouter';

I18nManager.allowRTL(true);

console.disableYellowBox = true;
//StatusBar.setHidden(false);
//StatusBar.setTranslucent(true)
//StatusBar.setBackgroundColor("#000000", true);
//StatusBar.set(true);

//containers
if (!__DEV__) {
  console.log = () => {};
  console.info = () => {};
}
const loggingMiddleware = store => next => action => {
  console.info('%cINFO:', action.type, `Dispatching a ${action.type} action with payload:`, action.payload);
  const result = next(action);
  console.info('%cNext State:', store.getState());
  return result;
};

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  stateReconciler: autoMergeLevel2,
  timeout: null
};

const persistedReducer = persistReducer(persistConfig, rootReducer);
const middleware = applyMiddleware(thunk, loggingMiddleware);
export const store = createStore(persistedReducer, {}, middleware);
let persistor = persistStore(store);
const ConnectedRouter = connect()(AppRouter);

Text.defaultProps = Text.defaultProps || {};
Text.defaultProps.allowFontScaling = false;

const app = () => (
  <View style={{ flex: 1 }}>
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <LanguageProvider strings={Strings} language="en">
          <ConnectedRouter />
        </LanguageProvider>
      </PersistGate>
    </Provider>

    <DropdownAlert
      defaultContainer={{ padding: 8, paddingTop: StatusBar.currentHeight, flexDirection: 'row' }}
      ref={ref => AlertHelper.setDropDown(ref)}
      infoColor={'#8147e0'}
      showCancel
      onClose={() => AlertHelper.invokeOnClose()}
      infoImageSrc={OrderNotificationIcon}
    />
  </View>
);

export default app;
