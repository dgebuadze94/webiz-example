// @flow

import {
  DATA_SUCCESS,
  CLUB_PRODUCTS_SUCCESS,
  CLUB_DASHBOARD_SUCCESS,
  CLUB_SCROLL_TO_TOP,
  TOGGLE_NAVIGATION_MODAL
} from '../../constants/action-types';


export const Success = responseData => ({
  type: DATA_SUCCESS,
  payload: { responseData }
});

export const clubProductsSuccess = products => ({
  type: CLUB_PRODUCTS_SUCCESS,
  payload: { products }
});

export const clubDashboardSuccess = data => ({
  type: CLUB_DASHBOARD_SUCCESS,
  payload: data
});

export const clubScrollToTop = () => ({
  type: CLUB_SCROLL_TO_TOP,
  payload: { timeStamp: Date.now() }
});

export const toggleNavigationModal = () => ({
  type: TOGGLE_NAVIGATION_MODAL
});
