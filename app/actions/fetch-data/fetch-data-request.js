import {
  CLUB_PRODUCTS_REQUEST,
  CLUB_DASHBOARD_REQUEST,
} from '../../constants/action-types';

export const clubProductsRequest = () => ({
  type: CLUB_PRODUCTS_REQUEST
});

export const clubDashboardRequest = () => ({
  type: CLUB_DASHBOARD_REQUEST
});
