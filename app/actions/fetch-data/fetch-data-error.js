// @flow

import {
  DATA_ERROR,
  CLUB_PRODUCTS_ERROR,
  CLUB_DASHBOARD_ERROR,
} from '../../constants/action-types';

export const clubProductsError = () => ({
  type: CLUB_PRODUCTS_ERROR
});

export const clubDashboardError = () => ({
  type: CLUB_DASHBOARD_ERROR
});

export const Error = () => ({
  type: DATA_ERROR,
  payload: { error: true }
});
