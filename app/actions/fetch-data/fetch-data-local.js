import {
  CLUB_PRODUCTS_LOCAL,
  PRODUCT_DETAILS_LOCAL,
  CLUB_CATEGORIES_LOCAL,
  SEEN_PENDING
} from '../../constants/action-types';

export const clubProductsLocal = products => ({
  type: CLUB_PRODUCTS_LOCAL,
  payload: { products }
});

export const productDetailsLocal = productDetails => ({
  type: PRODUCT_DETAILS_LOCAL,
  payload: { productDetails }
});

export const clubCategoriesLocal = categories => ({
  type: CLUB_CATEGORIES_LOCAL,
  payload: { categories }
});

export const seenPending = seenPending => ({
  type: SEEN_PENDING,
  payload: { seenPending }
});
