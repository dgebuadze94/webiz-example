import {
  fetchClubProducts,
  fetchClubDashboard,
} from '../../services/home-requests';

import { saveLanguageJson, getLanguage, localCardData, localCardDataStore } from '../../services/home-local';

import {
  clubDashboardError
} from './fetch-data-error';

import {
  Request,
  clubDashboardRequest
} from './fetch-data-request';

import { seenPending } from './fetch-data-local';

import {
  clubDashboardSuccess
} from './fetch-data-success';

export const getClubProducts = data => async dispatch => {
  dispatch(clubProductsRequest());
  try {
    const responseData = await fetchClubProducts(data);
    dispatch(clubProductsSuccess(responseData));
  } catch (e) {
    return dispatch(clubProductsError());
  }
};

export const getClubDashboard = data => async dispatch => {
  dispatch(clubDashboardRequest());
  try {
    const responseData = await fetchClubDashboard(data);
    dispatch(clubDashboardSuccess(responseData));
  } catch (e) {
    return dispatch(clubDashboardError());
  }
};
