import React from 'react';
import { AsyncStorage } from 'react-native';

export default class Auth {
	user_id = '';
	user_name = '';
	token = '';
	user = '';
	isRegister = false;
	static sharedInstance = null;

	static create(user_id, token, user = '') {
		if (Auth.sharedInstance === null) {
			Auth.sharedInstance = new Auth();
		}
		Auth.sharedInstance.user_id = user_id;
		Auth.sharedInstance.token = token;
		Auth.sharedInstance.user = user;
		if (user_id) {
			AsyncStorage.multiSet([['user_id', user_id], ['token', token]]);
		}
	}

	static remove() {
		Auth.sharedInstance = null;
		var keys = ['token'];
		AsyncStorage.multiRemove(keys, err => {});
	}

	static removeAll() {
		Auth.sharedInstance = null;
		var keys = ['token', 'user_id', 'user_name'];
		AsyncStorage.multiRemove(keys, err => {});
	}

	static getUserId() {
		if (Auth.sharedInstance) {
			if (Auth.sharedInstance.user_id) {
				return Auth.sharedInstance.user_id;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	static getUserDetails() {
		if (Auth.sharedInstance) {
			if (Auth.sharedInstance.user) {
				return Auth.sharedInstance.user;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	static getToken() {
		if (Auth.sharedInstance && Auth.sharedInstance.token) {
			return Auth.sharedInstance.token;
		} else {
			return null;
		}
	}

	static getShared() {
		if (Auth.sharedInstance === null) {
			return false;
		} else {
			return Auth.sharedInstance;
		}
	}

	static SetIsRegister(isRegister = true) {
		if (Auth.sharedInstance === null) {
			Auth.sharedInstance = new Auth();
		}
		Auth.sharedInstance.isRegister = isRegister;
	}
	static GetIsRegister() {
		if (Auth.sharedInstance === null) {
			return false;
		}
		return Auth.sharedInstance.isRegister;
	}
}
