import React, { Component } from 'react';
import { Text, View, ScrollView, SafeAreaView, I18nManager, Platform, StyleSheet } from 'react-native';

import { Actions } from 'react-native-router-flux';
import connect from '../../languageProvider/connect';
import Header from '../../reusableComponents/tabbar/Header';
import NavigationModal from '../../reusableComponents/NavigationModal/NavigationModal';

export class About extends Component {
  render() {
    let { strings, NavModal, toggleNavigationModal } = this.props;
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
        <View style={styles.container}>
          <Header
            language={this.props.language}
            title={strings.about_us}
            displayLeftAsBackButton
            shouldDisplayUnderline
            undelineColor={'#f5f6f8'}
            underlineWidth={1}
            onPressLeftView={() => Actions.pop()}
            rightView={drawerImage}
            rightViewStyle={{
              width: 18,
              transform: I18nManager.isRTL ? [{ rotate: '180deg' }] : [{ rotate: '0deg' }]
            }}
            onPressRightView={toggleNavigationModal}
          />
          <ScrollView bounces={false}>
            <View style={styles.mainSection}>
              <Text style={styles.mainSectionHeading1}>{strings.we_are}</Text>
              <Text style={styles.mainSectionHeading2}>{strings.visionaries_inventors_etc}</Text>
            </View>
            <View style={styles.section}>
              <Text style={styles.sectionHeading}>{strings.a_little_bit_about_us}</Text>
              <Text style={[styles.sectionContent]}>{strings.we_all_love_to_shop_etc}</Text>
            </View>
            <View style={styles.section}>
              <Text style={styles.sectionHeading}>{strings.what_is_testAPP}</Text>
              <Text style={styles.sectionContent}>{strings.testAPP_is_etc}</Text>
            </View>
            <View style={styles.sectionHeader}>
              <Text style={styles.sectionHeaderText}>{strings.here_how_it_works}</Text>
            </View>

            <View style={styles.bottomSection}>
              <Text style={styles.sectionHeading}>{strings.quick_and_easy}</Text>
              <Text style={styles.sectionContent}>{strings.with_testAPP_etc}</Text>
              <Text style={[styles.sectionHeading, { marginTop: 20 }]}>{strings.be_safe_and_secure}</Text>
              <Text style={styles.sectionContent}>{strings.our_biometric_id_etc}</Text>
              <Text style={[styles.sectionHeading, { fontSize: 18, marginTop: 20 }]}>
                {strings.control_your_digital}
              </Text>
              <Text style={styles.sectionContent}>{strings.testAPP_full_transparency}</Text>
            </View>
            <View style={styles.footer}>
              <Text style={styles.footerText}>{strings.find_how_to_profit}</Text>
            </View>
          </ScrollView>
        </View>
        <NavigationModal visible={NavModal} toggleNavigationModal={toggleNavigationModal} />
      </SafeAreaView>
    );
  }
}

export default connect(About);

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: '#F5F6F8'
  },
  header: {
    backgroundColor: '#ffffff',
    height: 71,
    alignItems: 'center',
    flexDirection: 'row',
    borderColor: '#F5F6F8',
    borderBottomWidth: 1
  },
  backArrowContainer: {
    width: '15%',
    justifyContent: 'center'
  },
  backArrow: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  backArrowImage: {
    height: 18,
    width: 18,
    resizeMode: 'contain',
    transform: I18nManager.isRTL ? [{ rotate: '180deg' }] : [{ rotate: '0deg' }]
  },
  headingContainer: {
    width: '70%',
    flexDirection: 'row',
    justifyContent: 'center'
  },
  headingText: {
    fontSize: 17,
    color: 'rgb(50,44,59)',
    fontFamily: 'SourceSansPro-Semibold'
  },
  mainSection: {
    backgroundColor: '#ffffff',
    height: 200,
    paddingTop: 51,
    paddingHorizontal: 30,
    textAlign: I18nManager.isRTL ? 'right' : 'left'
  },
  mainSectionHeading1:
    Platform.OS === 'ios'
      ? {
          fontSize: 23,
          color: 'rgba(57,49,67,0.22)',
          lineHeight: 28,
          fontFamily: 'BrandonGrotesque-Black',
          textAlign: 'left'
        }
      : {
          fontSize: 23,
          color: 'rgba(57,49,67,0.22)',
          lineHeight: 28,
          fontFamily: 'BrandonGrotesque-Black'
        },
  mainSectionHeading2:
    Platform.OS === 'ios'
      ? {
          fontSize: 23,
          lineHeight: 28,
          color: 'rgba(57,49,67,1)',
          fontFamily: 'BrandonGrotesque-Black',
          paddingTop: 12,
          textAlign: 'left'
        }
      : {
          fontSize: 23,
          lineHeight: 28,
          color: 'rgba(57,49,67,1)',
          fontFamily: 'BrandonGrotesque-Black',
          paddingTop: 12
        },
  section: {
    marginTop: 6,
    backgroundColor: '#ffffff',
    paddingTop: 33,
    paddingHorizontal: 29,
    paddingBottom: 29
  },
  sectionHeading:
    Platform.OS === 'ios'
      ? {
          fontSize: 19,
          lineHeight: 28,
          color: 'rgba(57,49,67,1)',
          fontFamily: 'BrandonGrotesque-Black',
          textAlign: 'left'
        }
      : {
          fontSize: 19,
          lineHeight: 28,
          color: 'rgba(57,49,67,1)',
          fontFamily: 'BrandonGrotesque-Black'
        },
  sectionContent:
    Platform.OS === 'ios'
      ? {
          fontSize: 16,
          lineHeight: 23,
          color: '#3A3A54',
          fontFamily: 'OpenSans-Regular',
          textAlign: 'left'
        }
      : {
          fontSize: 16,
          lineHeight: 23,
          color: '#3A3A54',
          fontFamily: 'OpenSans-Regular',
          textAlign: 'left'
        },
  sectionHeader: {
    marginTop: 6,
    backgroundColor: '#ffffff',
    paddingHorizontal: 29
  },
  sectionHeaderText:
    Platform.OS === 'ios'
      ? {
          fontSize: 15,
          lineHeight: 28,
          paddingTop: 21,
          paddingBottom: 15,
          color: 'rgba(57,49,67,1)',
          fontFamily: 'BrandonGrotesque-Black',
          textAlign: 'left'
        }
      : {
          fontSize: 15,
          lineHeight: 28,
          paddingTop: 21,
          paddingBottom: 15,
          color: 'rgba(57,49,67,1)',
          fontFamily: 'BrandonGrotesque-Black'
        },
  bottomSection: {
    marginTop: 1,
    backgroundColor: '#ffffff',
    paddingLeft: 29,
    paddingRight: 29,
    paddingBottom: 29,
    paddingTop: 22
  },
  footer: {
    marginTop: 6,
    backgroundColor: '#ffffff',
    paddingLeft: 29,
    paddingRight: 29
  },
  footerText:
    Platform.OS === 'ios'
      ? {
          fontSize: 15,
          lineHeight: 28,
          paddingTop: 21,
          paddingBottom: 15,
          color: 'rgba(57,49,67,1)',
          fontFamily: 'OpenSans-Regular',
          textAlign: 'left'
        }
      : {
          fontSize: 15,
          lineHeight: 28,
          paddingTop: 21,
          paddingBottom: 15,
          color: 'rgba(57,49,67,1)',
          fontFamily: 'OpenSans-Regular'
        }
});
