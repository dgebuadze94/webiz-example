import React, { Component } from 'react';
import { View, FlatList, Dimensions, StyleSheet, ScrollView } from 'react-native';
import { Actions } from 'react-native-router-flux';
import ProductItem from './productItem';
import SortAndFilter from './sortAndFilter';
import Strings from '../../../localization/strings';
import SortDrawerModal from './sortDrawerModal';
import ProductLoader from './productLoader';
import Loader from '../../../reusableComponents/loader';

const DEVICE_HEIGHT = Dimensions.get('window').height;
const SMALL_SCREEN = DEVICE_HEIGHT < 600;

class ProductGrid extends Component {
  state = {
    products: this.props.products,
    sortModalVisible: false,
    sortBy: { name: Strings.new, id: 0 }
  };
  sortOptions = [
    { name: Strings.new, id: 0 },
    { name: Strings.price_low, id: 1 },
    { name: Strings.price_high, id: 2 },
    { name: Strings.by_discount, id: 3 }
  ];

  componentDidUpdate(prevProps) {
    if (prevProps.products !== this.props.products) {
      this.setState({ products: this.props.products });
    }
  }

  onSort = option => {
    let products = [...this.state.products];
    switch (option) {
      case 0:
        products.sort((a, b) => a.id - b.id);
        break;
      case 1:
        products = products.sort((a, b) => a.price - b.price);
        break;
      case 2:
        products = products.sort((a, b) => b.price - a.price);
        break;
      case 3:
        products = products.sort((a, b) => b.discount - a.discount);
        break;
    }
    this.setState({ products });
  };

  renderPlaceholders = index => {
    const style = [
      { width: '99.7%', height: '100%' },
      index % 2 === 0 && {
        borderRightWidth: 2,
        borderRightColor: 'rgba(245,246,248,1)'
      }
    ];
    return (
      <View style={[styles.product]}>
        <ProductLoader style={style} />
      </View>
    );
  };

  renderProduct = (data, index, loading) => {
    const style = [
      { width: '99.7%', height: '100%' },
      index % 2 === 0 && {
        borderRightWidth: 2,
        borderRightColor: 'rgba(245,246,248,1)'
      }
    ];
    return (
      <View style={[styles.product]}>
        <ProductItem
          style={style}
          data={data}
          onPress={() =>
            this.props.isFromMore
              ? Actions.refresh({
                  key: Math.random(),
                  productId: data.id,
                  data: data
                })
              : debounceRoute.action('product', {
                  productId: data.id,
                  data: data
                })
          }
        />
      </View>
    );
  };

  render() {
    const {
      style,
      withSort,
      withFilter,
      scrollEnabled = true,
      isFromStore,
      loadMore,
      listHeaderComponent,
      reference,
      loading,
      page
    } = this.props;
    // const data = loading ? Array.from({ length: 6 }, (v, i) => i) : this.state.products;
    const data = this.state.products;

    return (
      <>
        {withSort && (
          <SortAndFilter
            onSort={() => this.setState({ sortModalVisible: true })}
            onFilter={() => alert('filter')}
            sortValue={this.state.sortBy.name}
            shouldDisplayFilter={withFilter}
          />
        )}
        {
          //!DO NOT CHANGE TO ONE FLATLIST!
        }
        {loading && page <= 1 ? (
          <FlatList
            data={Array.from({ length: 6 }, (v, i) => i)}
            extraData={[loading, page]}
            numColumns={2}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => this.renderPlaceholders(index)}
            style={[styles.container, style]}
            contentContainerStyle={[styles.grid, style]}
            initialNumToRender={10}
            showsVerticalScrollIndicator={false}
            ListHeaderComponent={listHeaderComponent}
            ref={reference}
            // removeClippedSubviews
          />
        ) : (
          <FlatList
            data={data}
            extraData={[this.state, loading, page]}
            numColumns={2}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => this.renderProduct(item, index, loading)}
            style={[styles.container, style]}
            contentContainerStyle={[styles.grid, style]}
            onEndReached={data && loadMore && !loading ? loadMore : () => {}}
            onEndReachedThreshold={0.3}
            initialNumToRender={10}
            showsVerticalScrollIndicator={false}
            ListHeaderComponent={listHeaderComponent}
            ref={reference}
            // removeClippedSubviews
          />
        )}
        {loading && page > 1 && (
          <View style={styles.loader}>
            <Loader value={2} />
          </View>
        )}
        <SortDrawerModal
          visible={this.state.sortModalVisible}
          onClose={() => this.setState({ sortModalVisible: false })}
          title={Strings.sort_by}
          options={this.sortOptions}
          withHeader={!isFromStore}
          onChangeOption={option => {
            this.setState({ sortBy: option });
            this.onSort(option.id);
          }}
        />
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    backgroundColor: '#fff'
  },
  grid: {
    // flex: 1,
    paddingBottom: 100,
    backgroundColor: 'rgba(245,246,248,1)'
  },
  product: {
    width: '50%',
    height: SMALL_SCREEN ? 232 : 246
  },
  loader: {
    position: 'absolute',
    bottom: 50,
    left: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center'
  }
});

export default ProductGrid;
