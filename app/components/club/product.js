import React, { Component } from 'react';
import ReactNative, {
  Text,
  View,
  SafeAreaView,
  ScrollView,
  Image,
  TouchableOpacity,
  Platform,
  StyleSheet,
  I18nManager,
  Linking,
  Animated,
  Easing,
  UIManager,
  BackHandler,
  Dimensions
} from 'react-native';
import DeviceInfo from 'react-native-device-info';
import { Actions } from 'react-native-router-flux';
import Share from 'react-native-share';
import FastImage from 'react-native-fast-image';
import connect from '../../languageProvider/connect';
import backBtn from '../../../images/club/backBtn.png';
import optionArrow from '../../../images/club/backArrow.png';
import discountIcon from '../../../images/club/discount.png';
import discountBanner from '../../../images/club/discountBanner.png';
import ProductGrid from './subComponents/productGrid';
import Loader from '../../reusableComponents/loader';
import Carousel from 'react-native-snap-carousel';
import PayButton from '../../containers/club/payButton';

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;
const SMALL_SCREEN = DEVICE_HEIGHT < 600;

export class Product extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      description: '',
      imageIndex: 0,
      price: '',
      old_price: '',
      discount: '',
      currency_icon: '',
      store_domain: '',
      product_url: '',
      options: {},
      shipping: null,
      selectedOptions: [],
      shouldLoadSuggestions: false,
      related: []
    };
    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
    this.scaleValue = new Animated.Value(0);
  }
  async componentDidMount() {
    let cachedProduct = this.props.productData.productDetails[this.props.productId]
      ? this.props.productData.productDetails[this.props.productId].product
      : {};
    this.setState({ ...cachedProduct });
    await this.props.getProductDetails({ id: this.props.productId });
    let product = this.props.productData.productDetails[this.props.productId].product;
    let store_domain = product.store_domain ? product.store_domain.replace('https://', '').replace('www.', '') : ' ';
    store_domain = store_domain[0].toUpperCase() + store_domain.substring(1);
    this.setState({ ...product, store_domain });
    const { options } = product;
    this.hasOption('color') === 1 && this.props.changeColor(options[0].values[0]);
    this.hasOption('size') === 1 && this.props.changeSize(options[1].values[0]);
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      Actions.pop();
      return true;
    });
  }
  componentWillUnmount() {
    this.props.resetOptions();
    this.backHandler.remove();
  }

  componentDidUpdate(prevProps) {
    if (
      prevProps.selectedVariation !== this.props.selectedVariation ||
      (prevProps.selectedVariation && prevProps.selectedVariation.id !== this.props.selectedVariation.id)
    ) {
      const variation = this.props.selectedVariation;
      const old_price = variation && variation.regular_price ? variation.regular_price : this.state.old_price;
      const price = variation && variation.sale_price ? variation.sale_price : this.state.price;
      this.setState({ price, old_price });
    }
  }

  onScroll = async event => {
    if (event.nativeEvent.contentOffset.y >= 300) {
      this.setState({ shouldLoadSuggestions: true });
    }
  };
  shareHandler = option => {
    if (option === 'TWITTER') {
      const twitterUrlScheme = `twitter://post?message=${this.state.product_url}`;
      Linking.canOpenURL(twitterUrlScheme)
        .then(supported =>
          Linking.openURL(
            supported ? twitterUrlScheme : `https://www.twitter.com/intent/tweet?text=${this.state.product_url}`
          )
        )
        .catch(err => console.error('An error occurred', err));
    } else {
      let shareOptions = {
        title: 'Share via',
        message: 'some message',
        url: this.state.product_url,
        social: Share.Social[option]
      };
      Share.shareSingle(shareOptions);
    }
  };
  hasOption = option => {
    const { options } = this.state;
    let hasOption = options.find(item => item.name === option);
    hasOption = hasOption.values.length;
    return hasOption;
  };
  renderOptions = () => {
    const { options, images, shipping, currency_icon, variations, description } = this.state;
    const { size_and_color, size, color, qty, item_description, delivery_options } = this.props.strings;
    if (options) {
      let hasSize = this.hasOption('size') > 0;
      let hasColor = this.hasOption('color') > 0;
      const hasDescription = this.state.description;
      const sizeAndColorLabel = hasSize && hasColor ? size_and_color : hasSize ? size : color;
      const sizeAndColorValue =
        this.props.selectedColor +
        (this.props.selectedColor && this.props.selectedSize ? ',' : '') +
        this.props.selectedSize;

      const cardScale = this.scaleValue.interpolate({
        inputRange: [0, 1],
        outputRange: [1, 1.1]
      });
      let transformStyle = { transform: [{ scale: cardScale }] };

      return (
        <View>
          <View ref={e => (this.optionsRef = e)}>
            <Animated.View style={[transformStyle]}>
              {(hasSize || hasColor) &&
                this.renderOption('productOptions', sizeAndColorLabel, sizeAndColorValue, {
                  options,
                  image: images[0],
                  shipping: { ...shipping, currency: currency_icon },
                  variations
                })}
            </Animated.View>
          </View>
          {!hasColor &&
            !hasSize &&
            this.renderOption('productOptions', qty, this.props.selectedQuantity, {
              options,
              image: images[0],
              shipping: { ...shipping, currency: currency_icon },
              variations
            })}

          {hasDescription && this.renderOption('productDescription', item_description, '', { description })}
          {this.renderOption('deliveryOptions', delivery_options, '', {
            shipping,
            currency: currency_icon,
            id: this.state.id
          })}
        </View>
      );
    }
  };
  renderOption = (action, label = '', value = '', dataToSend) => {
    return (
      <TouchableOpacity
        style={styles.optionContainer}
        onPress={() => debounceRoute.action(action, { data: dataToSend })}
      >
        <View style={{ flexDirection: 'row' }}>
          <Text style={styles.optionLabel}>
            {label}
            {label === 'Size & Color' || label === 'Size' || label === 'Color' || label === 'Qty'}{' '}
          </Text>
          <Text style={styles.optionLabel}>{value !== '' ? `: ` : ''} </Text>
          <Text style={[styles.optionLabel, { color: 'rgba(102,102,102,1)' }]}> {value !== '' && value} </Text>
        </View>
        <Image source={optionArrow} style={styles.optionArrowImage}></Image>
      </TouchableOpacity>
    );
  };

  renderShareOption = (option, link) => {
    return (
      <TouchableOpacity onPress={() => this.shareHandler(link)}>
        <Image source={option} style={styles.shareIconImage} />
      </TouchableOpacity>
    );
  };

  renderImage({ item }) {
    return (
      <View>
        <FastImage style={styles.mainImage} source={{ uri: item }} resizeMode={'contain'} />
      </View>
    );
  }
  renderCarouselIndicatorImage(index) {
    const color = this.state.imageIndex === index ? 'rgb(128, 132, 233)' : 'rgb(216,216,216)';
    return <View style={[styles.carouselIndicator, { backgroundColor: color }]} key={index} />;
  }
  renderDiscountBanner = () =>
    this.state.discount > 0 && this.state.shipping === 0 ? (
      <View style={styles.discountBannerContainer}>
        <Image source={discountBanner} style={styles.discountBanner} />
        <View style={[styles.dicsountIconContainer, I18nManager.isRTL ? { right: 20 } : { left: 20 }]}>
          <Text style={styles.discountBannerText}> {this.state.discount}</Text>
          <Image source={discountIcon} style={styles.discountIcon} />
        </View>
      </View>
    ) : null;

  bounceItem = () => {
    const cardRef = this.optionsRef;

    cardRef.measureLayout(ReactNative.findNodeHandle(this.scrollView), (x, y) => {
      this.scrollView.scrollTo({ x: 0, y: y - 100, animated: true });
    });
    this.scaleValue.setValue(0);
    Animated.sequence([
      Animated.timing(this.scaleValue, {
        toValue: 1,
        duration: 350,
        easing: Easing.linear,
        useNativeDriver: true
      }),
      Animated.timing(this.scaleValue, {
        toValue: 0,
        duration: 350,
        easing: Easing.linear,
        useNativeDriver: true
      })
    ]).start();
  };

  render() {
    const { strings, language } = this.props;
    let { name, description, price, old_price, discount, currency_icon, related } = this.state;
    price = this.props.selectedQuantity ? (Number(price) * Number(this.props.selectedQuantity)).toFixed(2) : price;
    old_price = this.props.selectedQuantity
      ? (Number(old_price) * Number(this.props.selectedQuantity)).toFixed(2)
      : old_price;
    let store_domain = this.state.store_domain
      ? this.state.store_domain.replace('https://', '').replace('www.', '')
      : ' ';
    store_domain = store_domain[0].toUpperCase() + store_domain.substring(1);
    return name == '' ? (
      <View style={styles.loader}>
        <Loader value={2} />
      </View>
    ) : (
      <SafeAreaView style={styles.safeArea}>
        <TouchableOpacity style={styles.backButton} onPress={() => Actions.pop()}>
          <Image source={backBtn} style={styles.backImage} />
        </TouchableOpacity>
        <ScrollView
          style={styles.container}
          onScroll={this.onScroll}
          scrollEventThrottle={16}
          ref={e => (this.scrollView = e)}
        >
          <View style={styles.carousel}>
            <Carousel
              ref={c => {
                this.carousel = c;
              }}
              data={this.state.images}
              renderItem={this.renderImage}
              sliderWidth={DEVICE_WIDTH}
              itemWidth={DEVICE_WIDTH}
              loop={true}
              enableSnap={true}
              lockScrollWhileSnapping={true}
              onSnapToItem={index => {
                this.setState({ imageIndex: index });
              }}
            />
            {this.state.images.length > 1 && (
              <View
                style={[
                  styles.carouselIndicatorWrapper,
                  this.state.discount > 0 && this.state.shipping === 0 && { marginBottom: 56 }
                ]}
              >
                {this.state.images.map((image, index) => this.renderCarouselIndicatorImage(index))}
              </View>
            )}
            {this.renderDiscountBanner()}
          </View>
          <View style={styles.details}>
            <View style={{ paddingHorizontal: 24 }}>
              <Text style={styles.webSite}>{store_domain}</Text>
              <Text style={styles.name}>{name}</Text>
              <Text numberOfLines={3} style={styles.description}>
                {description}
              </Text>
              <View style={styles.priceContainer}>
                <View>
                  <View style={{ flexDirection: 'row' }}>
                    {discount > 0 ? (
                      <Text style={styles.oldPrice}>
                        {currency_icon}
                        {old_price}
                      </Text>
                    ) : (
                      <View />
                    )}
                    <Text style={styles.newPrice}>
                      {currency_icon}
                      {price}
                    </Text>
                  </View>
                </View>
                {discount > 0 ? (
                  <View style={styles.discountTextContainer}>
                    <Text style={styles.discountText}>-{discount}%</Text>
                  </View>
                ) : (
                  <View />
                )}
              </View>
            </View>

            <View style={[styles.optionsContainer, !related || (related.length === 0 && { marginBottom: 100 })]}>
              {this.renderOptions()}
            </View>
          </View>
          {related && related.length > 0 && (
            <View style={styles.productGrid}>
              <View style={styles.productGridHeadingContainer}>
                <Text style={styles.productGridHeading}>{strings.related_products}</Text>
              </View>
              <ProductGrid products={related} isFromMore />
            </View>
          )}
        </ScrollView>
        <PayButton redirect productData={this.props.productData.productDetails[this.props.productId].product} />
      </SafeAreaView>
    );
  }
}

export default connect(Product);

const styles = StyleSheet.create({
  loader: {
    flex: 1,
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F5F6F8'
  },
  safeArea: {
    flex: 1,
    backgroundColor: '#fff'
  },
  container: {
    flexGrow: 1,
    backgroundColor: 'rgba(235,236,236,1)'
  },
  header: {
    width: '100%',
    paddingHorizontal: 22,
    paddingTop: SMALL_SCREEN ? 28 : 38,
    paddingBottom: 5,
    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  carousel: {
    paddingBottom: 27,
    marginBottom: 11,
    backgroundColor: '#fff'
  },
  mainImage: {
    // height: SMALL_SCREEN ? 280 : 350
    width: '100%',
    aspectRatio: 1 / 1
  },
  details: {
    backgroundColor: '#fff',
    paddingTop: 25,
    marginBottom: 11
    // paddingBottom: 20
  },
  webSite: {
    fontFamily: 'Gilroy-Bold',
    fontSize: 15,
    color: 'rgba(49,46,52,0.61)',
    marginBottom: 10,
    textAlign: 'left'
  },
  name: {
    fontFamily: 'Gilroy-Bold',
    fontSize: 19,
    color: 'rgba(49,46,52,1)',
    marginBottom: 10,
    textAlign: 'left'
  },
  description: {
    fontFamily: 'OpenSans-Regular',
    fontSize: 14,
    color: '#000',
    textAlign: 'left'
  },
  priceContainer: {
    flexDirection: 'row',
    marginTop: 30,
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  priceHeading: {
    fontFamily: 'Gilroy-Medium',
    fontSize: 15,
    color: 'rgba(49,46,52,1)',
    marginBottom: 5
  },
  newPrice: {
    fontFamily: 'Gilroy-SemiBold',
    fontSize: 23,
    paddingTop: Platform.OS === 'ios' ? 5 : 0,
    color: '#000',
    marginRight: 11
  },
  oldPrice: {
    fontFamily: 'Gilroy-Medium',
    fontSize: 23,
    marginRight: 11,
    paddingTop: Platform.OS === 'ios' ? 5 : 0,
    color: 'rgba(168,168,168,1)',
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid',
    textDecorationColor: 'rgba(168,168,168,1)'
  },
  discountContainer: {
    width: 81.4,
    height: 49.5,
    borderRadius: 7,
    justifyContent: 'center',
    alignItems: 'center'
  },
  shadow: {
    shadowColor: 'rgba(107,63,223,0.25)',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 1,
    elevation: 2
  },
  discountText: {
    fontFamily: 'Gilroy-SemiBold',
    fontSize: 22,
    paddingTop: Platform.OS === 'ios' ? 8 : 0,
    color: '#000'
  },
  discountTextContainer: {
    borderStyle: 'dashed',
    borderWidth: 1,
    borderColor: '#807f93',
    borderRadius: 5,
    paddingTop: 8,
    paddingBottom: 8,
    paddingLeft: 13,
    paddingRight: 13,
    backgroundColor: 'rgba(218, 218 ,218, 0.28)'
  },
  optionsContainer: {
    marginTop: 37
  },
  optionLabel: {
    fontSize: 14,
    fontFamily: 'OpenSans-Regular',
    color: '#000'
  },
  payButton: {
    width: DEVICE_WIDTH - 40,
    height: 58,
    borderRadius: 7,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  payButtonText: {
    fontFamily: 'Gilroy-Bold',
    fontSize: 19,
    marginRight: 5,
    color: '#fff',
    paddingTop: Platform.OS === 'ios' ? 8 : 0
  },
  optionContainer: {
    width: '100%',
    height: 59,
    paddingHorizontal: 23,
    backgroundColor: '#fff',
    borderTopWidth: 1,
    borderTopColor: 'rgba(151,151,151,0.22)',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  optionValue: {
    color: 'rgba(102,102,102,1)'
  },
  optionArrowImage: {
    width: 15,
    height: 15,
    resizeMode: 'contain',
    opacity: 0.35,
    transform: I18nManager.isRTL ? [{ rotate: '0deg' }] : [{ rotate: '180deg' }]
  },
  logoImage: {
    width: 75,
    height: 20,
    resizeMode: 'contain'
  },
  sizeGuide: {
    fontFamily: 'OpenSans-Regular',
    fontSize: 14,
    color: 'rgba(0,0,0,0.69)',
    textDecorationLine: 'underline',
    textDecorationStyle: 'solid',
    textDecorationColor: 'rgba(0,0,0,0.69)'
  },
  shareContainer: {
    marginTop: 40
  },
  shareText: {
    fontFamily: 'Gilroy-Bold',
    fontSize: 15,
    color: 'rgba(49,46,52,1)'
  },
  shareIconsContainer: {
    marginTop: 17,
    flexDirection: 'row',
    width: '100%',
    height: 35,
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  shareIconImage: {
    width: 30,
    height: 30,
    resizeMode: 'contain'
  },
  productGrid: {
    width: '100%',
    flexGrow: 1,
    backgroundColor: '#fff',
    // marginBottom: 140
    marginBottom: 40
  },
  productGridHeadingContainer: {
    width: '100%',
    height: 53,
    paddingHorizontal: 19,
    flexDirection: 'row',
    alignItems: 'center'
  },
  productGridHeading: {
    fontFamily: 'Gilroy-Bold',
    fontSize: 16,
    color: 'rgba(49,46,52,1)'
  },
  dropDownContainer: {
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: '#000',
    borderRadius: 4,
    height: 48,
    marginBottom: 10
  },
  dropDown: {
    height: 44,
    paddingLeft: 19,
    fontFamily: 'OpenSans-Regular',
    fontSize: 13
  },
  carouselIndicator: {
    marginLeft: 6,
    height: 7,
    width: 7,
    borderRadius: 3.5,
    overflow: 'hidden'
  },
  carouselIndicatorWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10
  },
  footer: {
    width: '100%',
    height: 95,
    paddingTop: 11,
    backgroundColor: '#fff',
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
    alignSelf: 'flex-end',
    borderTopWidth: 1,
    borderTopColor: 'rgba(225,224,230,1)'
  },
  backButton: {
    width: 32,
    height: 32,
    position: 'absolute',
    top: DeviceInfo.hasNotch() ? 58 : 23,
    left: 22,
    zIndex: 1
  },
  backImage: {
    width: 32,
    height: 32,
    resizeMode: 'contain',
    transform: I18nManager.isRTL ? [{ rotate: '180deg' }] : [{ rotate: '0deg' }]
  },
  discountBannerContainer: {
    width: '100%',
    height: 56,
    position: 'absolute',
    bottom: Platform.OS === 'ios' ? -3 : 0
  },
  discountBanner: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain'
  },
  dicsountIconContainer: {
    height: 56,
    position: 'absolute',
    flexDirection: I18nManager.isRTL ? 'row-reverse' : 'row',
    alignItems: 'center',
    zIndex: 2
  },
  discountBannerText: {
    color: 'rgba(76,255,215,1)',
    fontSize: 44,
    fontFamily: 'Gilroy-Bold',
    textAlignVertical: 'center',
    paddingTop: Platform.OS === 'ios' ? 8 : 0
  },
  discountIcon: {
    maxWidth: 25
  }
});
