import React, { Component } from 'react';
import {
  Text,
  View,
  SafeAreaView,
  StyleSheet,
  ScrollView,
  Image,
  TouchableOpacity,
  Dimensions,
  FlatList,
  I18nManager
} from 'react-native';
import connect from '../../languageProvider/connect';
import Auth from '../../constants/Auth';
import WelcomeBanner from '../../containers/club/welcomeBanner';
import ProductGrid from './subComponents/productGrid';
import searchImage from '../../../images/club/search.png';
import discountBannerClubHome from '../../../images/club/discountBannerClubHome.png';
import backArrow from '../../../images/club/backArrow.png';

import drawerImage from '../../../images/club/drawer.png';
import drawerReverseImage from '../../../images/club/drawer-reverse.png';
import Header from '../../reusableComponents/tabbar/Header';
import { toggleNavigationModal } from '../../actions/fetch-data/fetch-data-success';
import LinearGradient from 'react-native-linear-gradient';

const DEVICE_HEIGHT = Dimensions.get('window').height;
const DEVICE_WIDTH = Dimensions.get('window').width;
const SMALL_SCREEN = DEVICE_HEIGHT < 600;
SectionHeader = ({ text, image, rightSection, id, style }) => {
  return (
    <LinearGradient
      style={[styles.sectionHeader, style]}
      colors={[
        'rgb(95,0,255)',
        'rgb(114,77,236)',
        'rgb(120,114,233)',
        'rgb(121,148,229)',
        'rgb(115,180,224)',
        'rgb(100,212,219)'
      ]}
      start={{ x: 0, y: 0 }}
      end={{ x: 1, y: 0 }}
    >
      <Text style={styles.sectionText}>{text.toUpperCase()}</Text>
      {rightSection && (
        <TouchableOpacity
          onPress={() => debounceRoute.action('category', { categoryId: id })}
        >
          <Text style={styles.viewAllText}>{rightSection}</Text>
        </TouchableOpacity>
      )}
    </LinearGradient>
  );
};

export class Club extends Component {
  constructor(props) {
    super(props);
    if (Auth.GetIsRegister()) {
      Auth.SetIsRegister(false);
    }
  }

  state = {
    categories: this.props.dashboard.data.categories || [],
    merchants: this.props.dashboard.data.merchants || [],
    top_products: this.props.dashboard.data.top_products || []
  };

  async componentDidMount() {
    await this.props.getData();
    const { categories, merchants, top_products } = this.props.dashboard.data;
    this.setState({ categories, merchants, top_products });
  }

  async componentDidUpdate(prevProps) {
    if (
      prevProps.dashboard.shouldRefresh !== this.props.dashboard.shouldRefresh &&
      this.props.dashboard.shouldRefresh
    ) {
      await this.props.getData();
      const { categories, merchants, top_products } = this.props.dashboard.data;
      this.setState({ categories, merchants, top_products });
    }

    if (
      prevProps.dashboard.scrollToTop &&
      this.props.dashboard.scrollToTop &&
      prevProps.dashboard.scrollToTop.timeStamp !== this.props.dashboard.scrollToTop.timeStamp &&
      this.props.dashboard.scrollToTop.status
    ) {
      this.scrollToTop();
    }
  }

  scrollToTop = () => {
    this.scrollView.scrollTo({ x: 0, y: 0, animated: true });
  };

  renderMerchant = (item, index) => {
    const style = [
      index % 2 === 0 && {
        borderRightWidth: 2,
        borderRightColor: 'rgba(245,246,248,1)'
      },
      index < 0 && { width: '100%' }
    ];
    const { store_id, store_image, store_name, description } = item;
    return (
      <TouchableOpacity
        activeOpacity={0.8}
        style={[styles.merchant, style]}
        onPress={() => debounceRoute.action('merchant', { id: store_id })}
        accessible={false}
        accessibilityLabel={`merchant_${store_id}`}
        testID={`merchant_${store_id}`}
      >
        <Image source={{ uri: store_image }} style={[styles.merchantImage, index < 0 && { height: 160 }]} />
        <Text style={styles.merchantText}>{description}</Text>
      </TouchableOpacity>
    );
  };

  renderStaticContent = () => {
    const { strings } = this.props;
    return (
      <View>
        <WelcomeBanner style={styles.welcomeBanner} text={strings.welcome_club} />
        <SectionHeader text={strings.top_discounts_for_you} image={discountBannerClubHome} style={{ marginTop: 10 }} />
      </View>
    );
  };

  render() {
    const { strings, toggleNavigationModal, language } = this.props;
    const { categories, merchants, top_products } = this.state;
    const Hebrew = language && language.language && language.language === 'he' ? true : false;

    return (
      <SafeAreaView style={styles.safeArea}>
        <View style={styles.container}>
          <View style={styles.searchBox}>
            <View style={styles.searchContainer}>
              <Image source={searchImage} style={styles.searchIcon} />
              <Text
                style={[styles.searchText, { marginLeft: Hebrew ? 0 : 40 }]}
                onPress={() => {
                  debounceRoute.action('categorySelection', { previousScene: 'club' });
                }}
              >
                {' '}
                {this.props.strings.search_products}
              </Text>
            </View>
            <TouchableOpacity onPress={toggleNavigationModal}>
              <Image
                source={
                  this.props.language.language && this.props.language.language === 'he'
                    ? drawerReverseImage
                    : drawerImage
                }
                style={{ width: 17, height: 15 }}
              />
            </TouchableOpacity>
          </View>

          <ScrollView style={styles.productGrid} ref={ref => (this.scrollView = ref)}>
            <ProductGrid
              products={top_products}
              listHeaderComponent={this.renderStaticContent}
              loading={this.props.dashboard.isLoading}
              style={{ paddingBottom: 0 }}
            />
            {categories.map(item => {
              return (
                <ProductGrid
                  products={item.products}
                  listHeaderComponent={() => (
                    <SectionHeader text={item.name} rightSection={strings.view_all} id={item.id} />
                  )}
                  loading={this.props.dashboard.isLoading}
                  style={{ paddingBottom: 0 }}
                />
              );
            })}
            {merchants.length > 0 && (
              <FlatList
                data={merchants.slice(1)}
                extraData={merchants}
                numColumns={2}
                keyExtractor={(item, index) => index.toString()}
                ListHeaderComponent={() => {
                  return (
                    <View>
                      <SectionHeader text={strings.top_merchants} />
                      {this.renderMerchant(merchants[0], -1)}
                    </View>
                  );
                }}
                renderItem={({ item, index }) => this.renderMerchant(item, index)}
                initialNumToRender={10}
                showsVerticalScrollIndicator={false}
              />
            )}
          </ScrollView>
        </View>
      </SafeAreaView>
    );
  }
}

export default connect(Club);

const styles = StyleSheet.create({
  loader: {
    flex: 1,
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  safeArea: {
    flex: 1,
    backgroundColor: '#fff'
  },
  container: {
    width: '100%',
    height: '100%',
    flex: 1
    // backgroundColor: 'rgba(245,246,248,1)'
  },
  welcomeBanner: {
    backgroundColor: 'rgba(255,255,255,1)',
    height: 270
  },
  categorySection: {
    width: '100%',
    // height: 100,
    // flexGrow: 1,
    alignItems: 'center',
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(49,46,52,0.1)',
    marginBottom: 5
  },
  categoriesContainer: {
    width: '90%',
    // flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'flex-start',
    paddingTop: SMALL_SCREEN ? 18 : 24
  },
  productGrid: {
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(245,246,248,1)'
    // marginBottom: SMALL_SCREEN ? 35 : 45
  },
  shoppingList: {
    backgroundColor: 'rgb(245, 246, 248)',
    marginBottom: 65
  },
  header: {
    width: '100%',
    paddingBottom: 10,
    paddingTop: 21,
    alignItems: 'center',
    zIndex: 1000,
    backgroundColor: 'darkblue'
  },
  searchBox: {
    width: '100%',
    height: 40,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    alignItems: 'center',
    marginVertical: 10
  },
  searchContainer: {
    width: '90%',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'rgba(245,246,248,1)',
    borderRadius: 10,
    paddingVertical: 7
  },
  searchIcon: {
    width: 16,
    height: 16,
    position: 'absolute',
    left: 10,
    bottom: 8
  },
  searchText: {
    color: 'rgba(168,168,168,1)',
    fontSize: 14,
    fontFamily: 'OpenSans-Regular',
    width: '35%'
  },
  bannerWrapper: { width: '100%' },
  sectionHeader: {
    width: '100%',
    height: 52,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
    paddingHorizontal: 19
  },
  sectionBackground: {
    width: '100%',
    height: 52,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0
  },
  sectionText: {
    fontFamily: 'Gilroy-Bold',
    fontSize: 16,
    color: '#fff',
    zIndex: 100,
    paddingTop: 5
  },
  merchant: {
    width: '50%',
    height: SMALL_SCREEN ? 232 : 246,
    borderBottomWidth: 2,
    borderBottomColor: 'rgba(245,246,248,1)',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    overflow: 'hidden',
    paddingBottom: 17
  },
  merchantImage: {
    width: '75%',
    height: 80
  },
  merchantText: {
    fontFamily: 'OpenSans-Regular',
    fontSize: 13,
    color: 'rgba(49,46,52,1)',
    position: 'absolute',
    bottom: 11
  },
  viewAllText: {
    fontFamily: 'OpenSans-Semibold',
    fontSize: 13,
    color: 'rgba(100,212,219,1)',
    justifyContent: 'center',
    paddingLeft: 4,
    paddingRight: 4,
    height: '100%'
  },
  nextArrow: {
    width: 7,
    height: 7,
    transform: [{ rotate: '180deg' }]
  }
});
