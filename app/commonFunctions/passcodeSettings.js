import { getPasscodeData, setPasscodeData } from '../services/home-local';

export const checkForPasscodeTimeout = async () => {
  const { tryCount, wrongPasscodeLastTime } = await getPasscodeData();
  const timeOutIndicator = JSON.parse(wrongPasscodeLastTime) + 2 ** (tryCount - 4) * 3 * 60000;
  const timeOutIndicatorWhenNotBlocked = JSON.parse(wrongPasscodeLastTime) + 3 * 60000;
  const isTimeOut =
    new Date(tryCount <= 5 ? timeOutIndicatorWhenNotBlocked : timeOutIndicator).getTime() <= new Date().getTime(); //TODO: Change to server time
  isTimeOut && tryCount <= 5 && resetWrongPasscodeData();
  return isTimeOut || tryCount < 5;
};

export const saveWrongPasscodeData = async () => {
  let { tryCount } = await getPasscodeData();
  tryCount = tryCount > 0 ? tryCount + 1 : 1;
  const serverTime = new Date().getTime(); //TODO: Change to server time
  await setPasscodeData(tryCount, serverTime);
  return tryCount;
};

export const resetWrongPasscodeData = async () => {
  await setPasscodeData(0, 0);
};
