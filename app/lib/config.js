let ROOT_KEY = 'https://api.example.com';

export const GET_ROOT = () => {
  return ROOT_KEY;
};

export const SET_ROOT = newRoot => {
  ROOT_KEY = newRoot;
};

export default {
  GET_CLUB_PRODUCTS_URL: 'club/products',
  GET_CLUB_DASHBOARD_URL: 'club/clubDashboard',
};
