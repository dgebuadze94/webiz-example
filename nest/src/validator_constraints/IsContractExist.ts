import {registerDecorator, ValidationOptions, ValidationError, ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments} from "class-validator";
import {Contracts} from "../entities/contracts.entity";
import {getToday} from "../modules/helper";


@ValidatorConstraint({ name: "message", async: true})
export class IsContractExistConstraint implements ValidatorConstraintInterface {

    validate(personID: any, args: ValidationArguments) {
        if(personID !== '') {
            return Contracts.findOne({
                where:`Contracts.termination_date >= '${getToday()}' AND Contracts.start_date <= '${getToday()}' AND Contracts.end_date >= '${getToday()}' AND person_id = '${personID}'`,
            }).then(contract => {
                if (contract) return false;
                return true;
            });
        }
        return true;
    }

}

export function IsContractExist(validationOptions?: ValidationOptions) {
    return function (object: Object, propertyName: string) {
        registerDecorator({
            target: object.constructor,
            propertyName: propertyName,
            options: validationOptions,
            constraints: [],
            validator: IsContractExistConstraint,
        });
    };
}
