import {registerDecorator, ValidationOptions, ValidationError, ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments} from "class-validator";
import {Interviews} from "../entities/interviews.entity";


@ValidatorConstraint({ name: "message", async: true})
export class IsInterviewIdExistConstraint implements ValidatorConstraintInterface {

    validate(interviewID: any, args: ValidationArguments) {
        if(interviewID !== '') {
            return Interviews.findOne({where: {id: interviewID}}).then(interview => {
                if (interview) return true;
                return false;
            });
        }
        return true;
    }

}

export function IsInterviewIdExist(validationOptions?: ValidationOptions) {
    return function (object: Object, propertyName: string) {
        registerDecorator({
            target: object.constructor,
            propertyName: propertyName,
            options: validationOptions,
            constraints: [],
            validator: IsInterviewIdExistConstraint,
        });
    };
}
