import {registerDecorator, ValidationOptions, ValidationError, ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments} from "class-validator";
import {Positions} from "../entities/positions.entity";

@ValidatorConstraint({ name: "message", async: true})
export class IsPositionIdExistConstraint implements ValidatorConstraintInterface {

    validate(positionID: any, args: ValidationArguments) {
        return Positions.findOne({where:{id:positionID}}).then(position => {
            if (position)  return true;
            return false;
        });
    }

}

export function IsPositionIdExist(validationOptions?: ValidationOptions) {
    return function (object: Object, propertyName: string) {
        registerDecorator({
            target: object.constructor,
            propertyName: propertyName,
            options: validationOptions,
            constraints: [],
            validator: IsPositionIdExistConstraint,
        });
    };
}
