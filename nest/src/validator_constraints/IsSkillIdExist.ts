import {registerDecorator, ValidationOptions, ValidationError, ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments} from "class-validator";
import {Skills} from "../entities/skills.entity";

@ValidatorConstraint({ name: "message", async: true})
export class IsSkillIdExistConstraint implements ValidatorConstraintInterface {

    validate(skillId: any, args: ValidationArguments) {
        return Skills.findOne({where:{id:skillId}}).then(skill => {
            if (skill)  return true;
            return false;
        });
    }

}

export function IsSkillIdExist(validationOptions?: ValidationOptions) {
    return function (object: Object, propertyName: string) {
        registerDecorator({
            target: object.constructor,
            propertyName: propertyName,
            options: validationOptions,
            constraints: [],
            validator: IsSkillIdExistConstraint,
        });
    };
}
