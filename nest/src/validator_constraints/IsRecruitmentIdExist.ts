import {registerDecorator, ValidationOptions, ValidationError, ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments} from "class-validator";
import {Recruitments} from "../entities/recruitments.entity";


@ValidatorConstraint({ name: "message", async: true})
export class IsRecruitmentIdExistConstraint implements ValidatorConstraintInterface {

    validate(recruitmentID: any, args: ValidationArguments) {
        if(recruitmentID !== '') {
            return Recruitments.findOne({where: {id: recruitmentID}}).then(recruitment => {
                if (recruitment) return true;
                return false;
            });
        }
        return true;
    }

}

export function IsRecruitmentIdExist(validationOptions?: ValidationOptions) {
    return function (object: Object, propertyName: string) {
        registerDecorator({
            target: object.constructor,
            propertyName: propertyName,
            options: validationOptions,
            constraints: [],
            validator: IsRecruitmentIdExistConstraint,
        });
    };
}
