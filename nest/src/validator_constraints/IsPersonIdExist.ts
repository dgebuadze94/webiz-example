import {registerDecorator, ValidationOptions, ValidationError, ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments} from "class-validator";
import {Persons} from "../entities/persons.entity";


@ValidatorConstraint({ name: "message", async: true})
export class IsPersonIdExistConstraint implements ValidatorConstraintInterface {

    validate(personID: any, args: ValidationArguments) {
        if(personID !== '') {
            return Persons.findOne({where: {id: personID}}).then(person => {
                if (person) return true;
                return false;
            });
        }
        return true;
    }

}

export function IsPersonIdExist(validationOptions?: ValidationOptions) {
    return function (object: Object, propertyName: string) {
        registerDecorator({
            target: object.constructor,
            propertyName: propertyName,
            options: validationOptions,
            constraints: [],
            validator: IsPersonIdExistConstraint,
        });
    };
}
