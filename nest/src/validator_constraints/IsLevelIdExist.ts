import {registerDecorator, ValidationOptions, ValidationError, ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments} from "class-validator";
import {SkillLevels} from "../entities/skillLevels.entity";

@ValidatorConstraint({ name: "message", async: true})
export class IsLevelIdExistConstraint implements ValidatorConstraintInterface {

    validate(levelID: any, args: ValidationArguments) {
        return SkillLevels.findOne({where:{id:levelID}}).then(level => {
            if (level)  return true;
            return false;
        });
    }

}

export function IsLevelIdExist(validationOptions?: ValidationOptions) {
    return function (object: Object, propertyName: string) {
        registerDecorator({
            target: object.constructor,
            propertyName: propertyName,
            options: validationOptions,
            constraints: [],
            validator: IsLevelIdExistConstraint,
        });
    };
}
