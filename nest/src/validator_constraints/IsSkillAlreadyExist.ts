import {registerDecorator, ValidationOptions, ValidationError, ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments} from "class-validator";
import {Skills} from "../entities/skills.entity";

@ValidatorConstraint({ name: "message", async: true})
export class IsSkillAlreadyExistConstraint implements ValidatorConstraintInterface {

    validate(skillName: any, args: ValidationArguments) {
        if(skillName !== '') {
            return Skills.findOne({where: {name: skillName}}).then(skill => {
                if (skill) return false;
                return true;
            });
        }
        return true;
    }

}

export function IsSkillAlreadyExist(validationOptions?: ValidationOptions) {
    return function (object: Object, propertyName: string) {
        registerDecorator({
            target: object.constructor,
            propertyName: propertyName,
            options: validationOptions,
            constraints: [],
            validator: IsSkillAlreadyExistConstraint,
        });
    };
}
