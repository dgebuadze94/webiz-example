import {registerDecorator, ValidationOptions, ValidationError, ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments} from "class-validator";
import {Contracts} from "../entities/contracts.entity";


@ValidatorConstraint({ name: "message", async: true})
export class IsContractIdExistConstraint implements ValidatorConstraintInterface {

    validate(contractID: any, args: ValidationArguments) {
        if(contractID !== '') {
            return Contracts.findOne({where: {id: contractID}}).then(contract => {
                if (contract) return true;
                return false;
            });
        }
        return true;
    }

}

export function IsContractIdExist(validationOptions?: ValidationOptions) {
    return function (object: Object, propertyName: string) {
        registerDecorator({
            target: object.constructor,
            propertyName: propertyName,
            options: validationOptions,
            constraints: [],
            validator: IsContractIdExistConstraint,
        });
    };
}
