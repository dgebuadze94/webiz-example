import {registerDecorator, ValidationOptions, ValidationError, ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments} from "class-validator";
import {Projects} from "../entities/projects.entity";

@ValidatorConstraint({ name: "message", async: true})
export class IsProjectIdExistConstraint implements ValidatorConstraintInterface {

    validate(projectID: any, args: ValidationArguments) {
        return Projects.findOne({where:{id:projectID}}).then(project => {
            if (project)  return true;
            return false;
        });
    }

}

export function IsProjectIdExist(validationOptions?: ValidationOptions) {
    return function (object: Object, propertyName: string) {
        registerDecorator({
            target: object.constructor,
            propertyName: propertyName,
            options: validationOptions,
            constraints: [],
            validator: IsProjectIdExistConstraint,
        });
    };
}
