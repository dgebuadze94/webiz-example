import {validate, IsEmail, ValidationError, IsNotEmpty, IsNumber, IsNumberString, IsDate, IsMobilePhone} from 'class-validator';
import {IsSkillAlreadyExist} from "../../validator_constraints/IsSkillAlreadyExist";
import {IsSkillIdExist} from "../../validator_constraints/IsSkillIdExist";
import {IsPersonIdExist} from "../../validator_constraints/IsPersonIdExist";
import {IsLevelIdExist} from "../../validator_constraints/IsLevelIdExist";


export class CreateSkillsDtoParams {

    @IsSkillIdExist({
        message: "Skill $value not exists."
    })
    id: number;

    @IsLevelIdExist({
        message: "Level $value not exists."
    })
    level_id:number;

}

export class CreateSkillsDtoBody {
    @IsSkillAlreadyExist({
        message: "Skill $value already exists. Choose another name."
    })
    name: string;

    @IsSkillIdExist({
        message: "Skill $value not exists."
    })
    skill_id: number;

    @IsPersonIdExist({
        message: "Person $value not exists."
    })
    person_id: number;

    @IsPersonIdExist({
        message: "Person $value not exists."
    })
    issued_by: number;

    @IsLevelIdExist({
        message: "Person $value not exists."
    })
    level_id: number;

}
