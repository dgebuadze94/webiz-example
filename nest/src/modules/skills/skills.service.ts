import {HttpException, HttpStatus, Injectable} from '@nestjs/common';
import {Skills} from "../../entities/skills.entity";
import {PersonSkills} from "../../entities/personSkills.entity";
import {SkillTypes} from "../../entities/skillTypes.entity";
import {SkillLevels} from "../../entities/skillLevels.entity";

const sortColumns = ['id','name'];

@Injectable()
export class SkillsService {

    async fetchSkills(dataPerPage=10,page=1,filters={}) {
        let sort = {};
        sort = {
            id:"DESC"
        };
        if(filters['sort'] && sortColumns.includes(filters['sort']['column'])) {
                sort = {
                    [filters['sort']['column']]: (filters['sort']['order']).toUpperCase()
            }
        }
        let options = {
            order: sort,
            relations:["type_id"],
            take: dataPerPage,
            skip: dataPerPage*page
        };
        delete filters['sort'];
        let queryArr = [];
        for(let filter in filters) {
            switch (filter) {
                case 'name':
                    queryArr.push(`Skills.${filter} LIKE '%${filters[filter]}%'`);
                    break;
                case 'keyWord':
                    queryArr.push(`Skills.name LIKE '%${filters[filter]}%'`);
                    break;
                default:
            }
        }
        options['where'] = queryArr.join(' AND ');
        return await Skills.findAndCount(options);
    }

    async fetchSkill(id:number) {
        let skill = await Skills.findOne({
            where: {
                id: id
            },
            relations:["type_id"],
        });
        if(skill === undefined){
            throw new HttpException({
                status: HttpStatus.NOT_FOUND,
                error: 'Data Not Found',
            }, 404);
        }
        return skill;
    }

    async addSkill(data:object) {
        let skills = new Skills();
        skills.name = data['name'];
        skills.type_id = data['type_id'];
        return await skills.save();
    }

    async editSkill(data:object) {
        let skillToUpdate = await Skills.findOne({
            where: {
                id: data['id']
            }
        });
        skillToUpdate.name = data['name'];
        skillToUpdate.type_id = data['type_id'];
        return await Skills.save(skillToUpdate);
    }

    async deleteSkill(id) {
        let deletePersonSkills = await PersonSkills.delete({ skill_id: id});
        let deleteSkill = await Skills.delete({id:id});
        if(deleteSkill === undefined || deleteSkill.affected === 0){
            throw new HttpException({
                status: HttpStatus.NOT_FOUND,
                error: 'Data Not Found',
            }, 404);
        }
        return deleteSkill;
    }
    async fetchPersonSkills(dataPerPage=10,page=1,filters={}) {
        let sort = {};
        sort = {
            id:"DESC"
        };
        if(filters['sort'] && sortColumns.includes(filters['sort']['column'])) {
            sort = {
                [filters['sort']['column']]: (filters['sort']['order']).toUpperCase()
            }
        }
        let options = {
            order: sort,
            relations: ["person_id","skill_id","issued_by","level_id"],
            take: dataPerPage,
            skip: dataPerPage*page
        };
        return await PersonSkills.findAndCount(options);
    }
    async fetchPersonSkill(id:number) {
        let skill = await PersonSkills.findOne({
            where: {
                id: id
            },
            relations: ["person_id","skill_id","skill_id.type_id","issued_by","level_id"],
        });
        if(skill === undefined){
            throw new HttpException({
                status: HttpStatus.NOT_FOUND,
                error: 'Data Not Found',
            }, 404);
        }
        return skill;
    }
    async addPersonSkill(data:object) {
        let personSkills = new PersonSkills();
        personSkills.person_id = data['person_id'];
        personSkills.skill_id = data['skill_id'];
        personSkills.valid_from =  data['valid_from'];
        personSkills.issued_by =  data['issued_by'];
        personSkills.level_id = data['level_id'];
        return await personSkills.save();
    }

    async editPersonSkill(data:object) {
        let personSkillToUpdate = await PersonSkills.findOne({
            where:{
                id: data['id']
            }
        });
        if(personSkillToUpdate === undefined){
            throw new HttpException({
                status: HttpStatus.NOT_FOUND,
                error: 'Data Not Found',
            }, 404);
        }
        personSkillToUpdate.person_id = data['person_id'];
        personSkillToUpdate.skill_id = data['skill_id'];
        personSkillToUpdate.valid_from =  data['valid_from'];
        personSkillToUpdate.issued_by =  data['issued_by'];
        personSkillToUpdate.level_id = data['level_id'];
        return PersonSkills.save(personSkillToUpdate);
    }

    async deletePersonSkill(id) {
        let deletePersonSkills = await PersonSkills.delete({ id: id});
        if(deletePersonSkills === undefined || deletePersonSkills.affected === 0){
            throw new HttpException({
                status: HttpStatus.NOT_FOUND,
                error: 'Data Not Found',
            }, 404);
        }
        return deletePersonSkills;
    }

    async fetchSkillsTypes(dataPerPage=10,page=1,filters={}) {
        let sort = {};
        sort = {
            id:"DESC"
        };
        if(filters['sort'] && sortColumns.includes(filters['sort']['column'])) {
            sort = {
                [filters['sort']['column']]: (filters['sort']['order']).toUpperCase()
            }
        }
        let options = {
            order: sort,
            take: dataPerPage,
            skip: dataPerPage*page
        };
        return await SkillTypes.findAndCount(options);
    }
    async fetchSkillsType(id:number) {
        let skill = await SkillTypes.findOne({
            where: {
                id: id
            }
        });
        if(skill === undefined){
            throw new HttpException({
                status: HttpStatus.NOT_FOUND,
                error: 'Data Not Found',
            }, 404);
        }
        return skill;
    }

    async editSkillsType(data:object) {
        let SkillTypeToUpdate = await SkillTypes.findOne({
            where:{
                id: data['id']
            }
        });
        if(SkillTypeToUpdate === undefined){
            throw new HttpException({
                status: HttpStatus.NOT_FOUND,
                error: 'Data Not Found',
            }, 404);
        }
        SkillTypeToUpdate.name = data['name'];
        SkillTypeToUpdate.order_id = data['order_id'];
        return SkillTypes.save(SkillTypeToUpdate);
    }

    async deleteSkillsType(id) {
        let deleteSkillTypes = await SkillTypes.delete({ id: id});
        if(deleteSkillTypes === undefined || deleteSkillTypes.affected === 0){
            throw new HttpException({
                status: HttpStatus.NOT_FOUND,
                error: 'Data Not Found',
            }, 404);
        }
        return deleteSkillTypes;
    }

    async fetchSkillsLevels(dataPerPage=10,page=1,filters={}) {
        let sort = {};
        sort = {
            id:"DESC"
        };
        if(filters['sort'] && sortColumns.includes(filters['sort']['column'])) {
            sort = {
                [filters['sort']['column']]: (filters['sort']['order']).toUpperCase()
            }
        }
        let options = {
            order: sort,
            take: dataPerPage,
            skip: dataPerPage*page
        };
        return await SkillLevels.findAndCount(options);
    }
    async fetchSkillsLevel(id:number) {
        let skill = await SkillLevels.findOne({
            where: {
                id: id
            }
        });
        if(skill === undefined){
            throw new HttpException({
                status: HttpStatus.NOT_FOUND,
                error: 'Data Not Found',
            }, 404);
        }
        return skill;
    }

    async editSkillsLevel(data:object) {
        let SkillLevelToUpdate = await SkillLevels.findOne({
            where:{
                id: data['id']
            }
        });
        if(SkillLevelToUpdate === undefined){
            throw new HttpException({
                status: HttpStatus.NOT_FOUND,
                error: 'Data Not Found',
            }, 404);
        }
        SkillLevelToUpdate.name = data['name'];
        SkillLevelToUpdate.order_id = data['order_id'];
        return SkillLevels.save(SkillLevelToUpdate);
    }

    async deleteSkillsLevel(id) {
        let deleteSkillLevels = await SkillLevels.delete({ id: id});
        if(deleteSkillLevels === undefined || deleteSkillLevels.affected === 0){
            throw new HttpException({
                status: HttpStatus.NOT_FOUND,
                error: 'Data Not Found',
            }, 404);
        }
        return deleteSkillLevels;
    }
}
