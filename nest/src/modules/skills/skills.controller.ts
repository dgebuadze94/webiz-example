import {Controller, Get, Post, Param, Res, Req, HttpStatus, HttpCode, Body, HttpService, Query} from '@nestjs/common';
import { Response } from 'express';

import {SkillsService} from "./skills.service";
import {CreateLeaveDtoParams} from "../leave/CreateLeaveDto";
import {CreateSkillsDtoBody, CreateSkillsDtoParams} from "./CreateSkillsDto";

@Controller('skills')
export class SkillsController {
    constructor(private readonly skillsService: SkillsService) {}

    @Post('add')
    async addSkill(@Body() body:CreateSkillsDtoBody, @Res() res: Response) {
        let addSkill = await this.skillsService.addSkill(body);
        return res.json({status:"success",result:{data:addSkill}});
    }

    @Post('edit/:id')
    async editSkill(@Param() params:CreateSkillsDtoParams, @Body() body:CreateSkillsDtoBody, @Res() res: Response) {
        body['id'] = params.id;
        let editSkill = await this.skillsService.editSkill(body);
        return res.json({status:"success",result:{data:editSkill}});
    }

    @Post('delete/:id')
    async deleteSkill(@Param() params, @Res() res: Response) {
        let deleteSkill = await this.skillsService.deleteSkill(params.id);
        return res.json({status:"success",result:{data:deleteSkill}});
    }

    @Post('add-person')
    async addPersonSkill(@Param() params:CreateSkillsDtoParams, @Body() body:CreateSkillsDtoBody, @Req() req, @Res() res: Response) {
        let addPersonSkill = await this.skillsService.addPersonSkill(body);
        return res.json({status:"success",result:{data:addPersonSkill}});
    }

    @Post('edit-person/:id')
    async editPersonSkill(@Param() params:CreateSkillsDtoParams, @Body() body:CreateSkillsDtoBody, @Req() req, @Res() res: Response) {
        body['id'] = params.id;
        let editPersonSkill = await this.skillsService.editPersonSkill(body);
        return res.json({status:"success",result:{data:editPersonSkill}});
    }

    @Post('persons')
    async fetchPersonSkills(@Query() query, @Body() body, @Res() res: Response){
        if(query.dataperpage === undefined){
            query.dataperpage = 10;
        }
        if(query.page === undefined){
            query.page = 1;
        }
        let skills = await this.skillsService.fetchPersonSkills(query.dataperpage,query.page-1);
        return res.json({status:"success",result:{data:skills[0],count:skills[1],dataPerPage:query.dataperpage, page:query.page}});
    }

    @Post('person/:id')
    async fetchPersonSkill(@Param() params, @Body() body, @Res() res: Response){
        let fetchPersonSkill = await this.skillsService.fetchPersonSkill(params.id);
        return res.json({status:"success",result:{data:fetchPersonSkill}});
    }


    @Post('delete-person/:id')
    async deletePersonSkill(@Param() params:CreateSkillsDtoParams, @Res() res: Response) {
        let deletePersonSkill = await this.skillsService.deletePersonSkill(params.id);
        return res.json({status:"success",result:{data:deletePersonSkill}});
    }

    @Post('types')
    async fetchSkillsTypes(@Query() query,@Param() params, @Body() body, @Res() res: Response){
        if(query.dataperpage === undefined){
            query.dataperpage = 10;
        }
        if(query.page === undefined){
            query.page = 1;
        }
        let fetchSkillsTypes = await this.skillsService.fetchSkillsTypes(query.dataperpage,query.page-1,body.filter);
        return res.json({status:"success",result:{data:fetchSkillsTypes[0],count:fetchSkillsTypes[1],dataPerPage:query.dataperpage, page:query.page}});
    }

    @Post('type/:id')
    async fetchSkillsType(@Param() params, @Body() body, @Res() res: Response){
        let fetchSkillsTypes = await this.skillsService.fetchSkillsType(params.id);
        return res.json({status:"success",result:{data:fetchSkillsTypes}});
    }

    @Post('delete-type/:id')
    async deleteSkillsTypes(@Param() params, @Res() res: Response) {
        let deleteSkillsTypes = await this.skillsService.deleteSkillsType(params.id);
        return res.json({status:"success",result:{data:deleteSkillsTypes}});
    }

    @Post('edit-level/:level_id')
    async editSkillsLevel(@Param() params:CreateSkillsDtoParams, @Body() body:CreateSkillsDtoBody, @Req() req, @Res() res: Response) {
        body['id'] = params.level_id;
        let editSkillsLevel = await this.skillsService.editSkillsLevel(body);
        return res.json({status:"success",result:{data:editSkillsLevel}});
    }

    @Post('levels')
    async fetchSkillsLevels(@Query() query, @Body() body, @Res() res: Response){
        if(query.dataperpage === undefined){
            query.dataperpage = 10;
        }
        if(query.page === undefined){
            query.page = 1;
        }
        let levels = await this.skillsService.fetchSkillsLevels(query.dataperpage,query.page-1);
        return res.json({status:"success",result:{data:levels[0],count:levels[1],dataPerPage:query.dataperpage, page:query.page}});
    }

    @Post('level/:level_id')
    async fetchSkillsLevel(@Param() params, @Body() body, @Res() res: Response){
        let fetchSkillsLevel = await this.skillsService.fetchSkillsLevel(params.level_id);
        return res.json({status:"success",result:{data:fetchSkillsLevel}});
    }


    @Post('delete-level/:level_id')
    async deleteSkillsLevel(@Param() params:CreateSkillsDtoParams, @Res() res: Response) {
        let deleteSkillsLevel = await this.skillsService.deleteSkillsLevel(params.level_id);
        return res.json({status:"success",result:{data:deleteSkillsLevel}});
    }

    @Post()
    async fetchSkills(@Query() query, @Body() body, @Res() res: Response) {
        if(query.dataperpage === undefined){
            query.dataperpage = 10;
        }
        if(query.page === undefined){
            query.page = 1;
        }
        let fetchSkills = await this.skillsService.fetchSkills(query.dataperpage,query.page-1,body.filter);
        return res.json({status:"success",result:{data:fetchSkills[0],count:fetchSkills[1],dataPerPage:query.dataperpage, page:query.page}});
    }

    @Post(':id')
    async fetchSkill(@Param() params:CreateSkillsDtoParams, @Res() res: Response){
        let skill = await this.skillsService.fetchSkill(params.id);
        return res.json({status:"success",result:{data:skill}});
    }
}
