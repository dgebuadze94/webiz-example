import { Injectable } from '@nestjs/common';
import * as multer from 'multer';
import * as AWS from 'aws-sdk';
import * as multerS3 from 'multer-s3';
import {Persons} from "../../entities/persons.entity";

const s3 = new AWS.S3();
AWS.config.update({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
});
@Injectable()
export class UploadService {

    cv = multer({
        fileFilter: function (req, file, cb) {
            let getFileType = (file.originalname.split(".").pop(-1)).toLowerCase();
            if (getFileType !== 'pdf' && getFileType !== 'docx' && getFileType !== 'doc') {
                return cb(new Error('Only pdfs, docx and doc are allowed'))
            }
            cb(null, true)
        },
        storage: multerS3({
            s3: s3,
            bucket: process.env.AWS_S3_BUCKET_NAME,
            acl: 'public-read',
            key: (request, file, cb) => {
                cb(null, `documents/${request.person_id}/${Date.now().toString()} - ${file.originalname}`);
            },
        }),
        limits : {
            fileSize : 15728640
        },
    }).array('cv');

    async fileUpload(req, res, personID) {
        try {
            req['person_id']=personID;
            this.cv(req, res, async (error) => {
                if (error) {
                    console.log(error);
                    return res.status(404).json(`Failed to upload file: ${error}`);
                }
                let personToUpdate = await Persons.findOne({
                    where: {
                        id: personID
                    }
                });
                personToUpdate.cv = req.files[0].location;
                await Persons.save(personToUpdate);
                return res.status(201).json({status:"success",result:{data:personToUpdate}});
            });
        } catch (error) {
            console.log(error);
            return res.status(500).json(`Failed to upload file: ${error}`);
        }
    }
}
