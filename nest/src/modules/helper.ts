export let getToday = () =>{
    let getDate = new Date();
    let dd = String(getDate.getDate()).padStart(2, '0');
    let mm = String(getDate.getMonth() + 1).padStart(2, '0');
    let yyyy = getDate.getFullYear();
    return yyyy + '-' + mm + '-' + dd;
};
