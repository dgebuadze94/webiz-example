import {validate, IsEmail, IsNotEmpty, IsNumber, IsNumberString, IsDate, IsMobilePhone} from 'class-validator';
import {Type} from "class-transformer";
import {IsPersonIdExist} from "../../validator_constraints/IsPersonIdExist";


export class CreatePersonDtoParams {

    @IsPersonIdExist({
        message: "Person $value not exists."
    })
    id: number;

}

export class CreatePersonDtoBody {

    @IsNumber()
    id: number;

    @IsEmail()
    email: string;

    @IsDate()
    @Type(() => Date)
    date_of_birth: Date;

}
