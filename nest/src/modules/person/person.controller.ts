import {Controller, Get, Post, Param, Res, Req, HttpStatus, HttpCode, Body, HttpService, Query} from '@nestjs/common';
import { Request, Response } from 'express';
import {PersonService} from './person.service';
import {ExperienceService} from '../experience/experience.service';
import {CreatePersonDtoParams,CreatePersonDtoBody} from './CreatePersonDto';
import {UploadService} from '../upload/upload.service';

@Controller('person')
export class PersonController {
    constructor(private readonly personService: PersonService,
                private readonly experienceService: ExperienceService,
                private readonly uploadService: UploadService) {}


    @Post('positions')
    async fetchPositions(@Res() res: Response) {
        let fetchPositions = await this.personService.fetchPositions();
        return res.json({status:"success",result:{data:fetchPositions[0],count:fetchPositions[1]}});
    }

    @Post('add')
    async addPerson(@Body() body, @Req() req, @Res() res: Response) {
        let addPerson = await this.personService.addPerson(body);
        return res.json({status:"success",result:{data:addPerson}});
    }

    @Post('edit/:id')
    async editPerson(@Param() params:CreatePersonDtoParams, @Body() body:CreatePersonDtoBody, @Req() req, @Res() res: Response) {
        body['id'] = params.id;
        let editPerson = await this.personService.editPerson(body);
        this.uploadService.fileUpload(req, res,editPerson.id);
        return res.json({status:"success",result:{data:editPerson}});
    }

    @Post('delete/:id')
    async deletePerson(@Param() params:CreatePersonDtoParams, @Res() res: Response) {
        let deletePerson = await this.personService.deletePerson(params.id);
        return res.json({status:"success",result:{data:deletePerson}});
    }

    @Post()
    async fetchPersons(@Query() query, @Body() body, @Res() res: Response){

        if(query.dataperpage === undefined){
            query.dataperpage = 10;
        }
        if(query.page === undefined){
            query.page = 1;
        }
        let persons = await this.personService.fetchPersons(query.dataperpage,query.page-1,body.filter);
        return res.json({status:"success",result:{data:persons[0],count:persons[1],dataPerPage:query.dataperpage, page:query.page}});
    }

    @Post(':id')
    async fetchPerson(@Param() params:CreatePersonDtoParams, @Res() res: Response){
        let person = await this.personService.fetchPerson(params.id);
        return res.json({status:"success",result:{data:person}});
    }

}
