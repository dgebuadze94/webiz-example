import {HttpStatus, HttpException} from '@nestjs/common';
import { Injectable } from '@nestjs/common';
import {getRepository, Like} from "typeorm";
import {getConnection} from 'typeorm';
import { Persons } from '../../entities/persons.entity';
import {Companies} from "../../entities/companies.entity";
import {Departments} from "../../entities/departments.entity";
import {CompanyDepartments} from "../../entities/companyDepartments.entity";
import {Positions} from "../../entities/positions.entity";
import {PersonPositions} from "../../entities/personPositions.entity";
import {PersonImages} from "../../entities/personImages.entity";
import {PersonSocialNetworks} from "../../entities/personSocialNetworks.entity";
import {Experiences} from "../../entities/experiences.entity";
import {Languages} from "../../entities/languages.entity";
import {ExperienceService} from "../experience/experience.service";
import {PersonSkills} from "../../entities/personSkills.entity";
import {Leaves} from "../../entities/leaves.entity";
import {Contracts} from "../../entities/contracts.entity";
import {getToday} from '../helper';
import {Recruitments} from "../../entities/recruitments.entity";
import {Educations} from "../../entities/educations.entity";
import {Works} from "../../entities/works.entity";
import {Families} from "../../entities/families.entity";

const sortColumns = ['first_name','last_name','phone','email'];

@Injectable()
export class PersonService {
    async addPerson(data:object) {
        let experienceService = new ExperienceService();
        let person = new Persons();
        person.first_name = data['first_name'];
        person.last_name = data['last_name'];
        return await person.save();
    }

    async editPerson(data:object) {
        let experienceService = new ExperienceService();
        let personToUpdate = await Persons.findOne({
            where: {
                id: data['id']
            }
        });
        personToUpdate.first_name = data['first_name'];
        personToUpdate.last_name = data['last_name'];
        personToUpdate.date_of_birth = data['date_of_birth'];
        personToUpdate.phone = data['phone'];
        personToUpdate.email = data['email'];
        personToUpdate.address = data['address'];
        personToUpdate.bio = data['bio'];
        personToUpdate.cv = data['cv'];
        personToUpdate.company_department_id = data['company_department_id'];
        if(data['position']) {
            await this.editPersonPosition(data['id'], data['position']);
        }
        if(data['experience']) {
            await experienceService.experienceWizard(data['id'], data['experience']);
        }
        if(data['skills']) {
            await this.skillsWizard(data['id'], data['skills']);
        }
        return await Persons.save(personToUpdate);
    }

   async fetchPersons(dataPerPage=10,page=1,filters={}) {
       let sort = {};
       sort = {
           "Persons.id":"DESC"
       };
       if(filters['sort'] && sortColumns.includes(filters['sort']['column'])) {
           sort = {
               [filters['sort']['column']]: (filters['sort']['order']).toUpperCase()
           }
       }
       let persons = await getRepository(Persons)
           .createQueryBuilder("Persons")
           .leftJoinAndSelect("Persons.company_department_id","companyDepartment")
           .leftJoinAndSelect("companyDepartment.company_id","company")
           .leftJoinAndSelect("companyDepartment.department_id","department")
           .leftJoinAndSelect("Persons.positions","positions")
           .leftJoinAndSelect("positions.position_id","position")
           .leftJoinAndSelect("Persons.languages","languages")
           .leftJoinAndSelect("languages.language_id","language")
           .leftJoinAndSelect("Persons.experience","experience")
           .leftJoinAndSelect("Persons.projects","projects")
           .leftJoinAndSelect("projects.project_id","project")
           .leftJoinAndSelect("Persons.skills","skills")
           .leftJoinAndSelect("skills.skill_id","skill")
           .leftJoinAndSelect("skills.level_id","level")
           .leftJoinAndSelect("skill.type_id","skillType")
           .leftJoinAndSelect("Persons.contracts","contracts",`contracts.termination_date >= :today AND contracts.start_date <= :today AND contracts.end_date >= :today`,{today:getToday()})
           .leftJoinAndSelect("contracts.position","contractPositions")
           .leftJoinAndSelect("Persons.recruitments","recruitment")
           .leftJoinAndSelect("Persons.bonuses","bonuses")
           .leftJoinAndSelect("bonuses.bonuse_id","bonus")
           .leftJoinAndSelect("Persons.deductions","deductions")
           .leftJoinAndSelect("deductions.deduction_id","deduction")
           .leftJoinAndSelect("Persons.works","works")
           .leftJoinAndSelect("works.position","workPosition")
           .leftJoinAndSelect("Persons.educations","educations")
           .leftJoinAndSelect("educations.degree","degree_type")
           .leftJoinAndSelect("Persons.families","families")
           .leftJoinAndSelect("families.relation_type","relation_type")
           .leftJoinAndSelect("Persons.leaves","leaves")
           .leftJoinAndSelect("leaves.type_id","leaveTypes")
           .leftJoinAndSelect("leaves.approved_by","leaveApprovedBy")
           .leftJoinAndSelect("leaves.registrant","leaveRegistrant")
           .leftJoinAndSelect("Persons.recruitments","recruitments")
           .leftJoinAndSelect("recruitments.expected_position","recruitmentExpectedPosition")
           .leftJoinAndSelect("recruitments.expected_project","recruitmentExpectedProject")
           .leftJoinAndSelect("recruitments.interviews","recruitmentInterviews")
           .leftJoinAndSelect("recruitmentInterviews.interviewers","recruitmentInterviewInterviewers")
           .leftJoinAndSelect("recruitmentInterviewInterviewers.person_id","recruitmentInterviewInterviewerPersons")
           .take(dataPerPage)
           .skip(dataPerPage*page)
           .orderBy(sort);

       for(let filter in filters) {
           switch (filter) {
               case 'first_name':
                   persons.andWhere(`Persons.${filter} LIKE :${filter}`,{[filter]: `%${filters[filter]}%`});
                   break;
               case 'last_name':
                   persons.andWhere(`Persons.${filter} LIKE :${filter}`,{[filter]: `%${filters[filter]}%`});
                   break;
               case 'phone':
                   persons.andWhere(`Persons.${filter} LIKE :${filter}`,{[filter]: `%${filters[filter]}%`});
                   break;
               case 'email':
                   persons.andWhere(`Persons.${filter} LIKE :${filter}`,{[filter]: `%${filters[filter]}%`});
                   break;
               case 'position':
                   persons.andWhere(`position.${filter} LIKE :${filter}`,{[filter]: `%${filters[filter]}%`});
                   break;
               case 'project':
                   persons.andWhere(`project.${filter} LIKE :${filter}`,{[filter]: `%${filters[filter]}%`});
                   break;
               case 'keyWord':
                   persons.orWhere(`Persons.first_name LIKE :first_name`,{first_name: `%${filters[filter]}%`});
                   persons.orWhere(`Persons.last_name LIKE :last_name`,{last_name: `%${filters[filter]}%`});
                   persons.orWhere(`Persons.phone LIKE :phone`,{phone: `%${filters[filter]}%`});
                   persons.orWhere(`Persons.email LIKE :email`,{email: `%${filters[filter]}%`});
                   persons.orWhere(`position.position LIKE :position`,{position: `%${filters[filter]}%`});
                   persons.orWhere(`project.project LIKE :project`,{project: `%${filters[filter]}%`});
                   break;
               default:
           }
       }
       return persons.getManyAndCount();
    }

    async fetchPerson(id:number) {
        let person = await getRepository(Persons)
            .createQueryBuilder("Persons")
            .where("Persons.id = :personID", { personID: id })
            .leftJoinAndSelect("Persons.company_department_id","companyDepartment")
            .leftJoinAndSelect("companyDepartment.company_id","company")
            .leftJoinAndSelect("companyDepartment.department_id","department")
            .leftJoinAndSelect("Persons.positions","positions")
            .leftJoinAndSelect("positions.position_id","position")
            .leftJoinAndSelect("Persons.languages","languages")
            .leftJoinAndSelect("languages.language_id","language")
            .leftJoinAndSelect("Persons.experience","experience")
            .leftJoinAndSelect("Persons.projects","projects")
            .leftJoinAndSelect("projects.project_id","project")
            .leftJoinAndSelect("Persons.skills","skills")
            .leftJoinAndSelect("skills.skill_id","skill")
            .leftJoinAndSelect("skills.level_id","level")
            .leftJoinAndSelect("skill.type_id","skillType")
            .leftJoinAndSelect("Persons.contracts","contracts",`contracts.termination_date >= :today AND contracts.start_date <= :today AND contracts.end_date >= :today`,{today:getToday()})
            .leftJoinAndSelect("contracts.position","contractPositions")
            .leftJoinAndSelect("Persons.recruitments","recruitment")
            .leftJoinAndSelect("Persons.bonuses","bonuses")
            .leftJoinAndSelect("bonuses.bonuse_id","bonus")
            .leftJoinAndSelect("Persons.deductions","deductions")
            .leftJoinAndSelect("deductions.deduction_id","deduction")
            .leftJoinAndSelect("Persons.works","works")
            .leftJoinAndSelect("works.position","workPosition")
            .leftJoinAndSelect("Persons.educations","educations")
            .leftJoinAndSelect("educations.degree","degree_type")
            .leftJoinAndSelect("Persons.families","families")
            .leftJoinAndSelect("families.relation_type","relation_type")
            .leftJoinAndSelect("Persons.leaves","leaves")
            .leftJoinAndSelect("leaves.type_id","leaveTypes")
            .leftJoinAndSelect("leaves.approved_by","leaveApprovedBy")
            .leftJoinAndSelect("leaves.registrant","leaveRegistrant")
            .leftJoinAndSelect("Persons.recruitments","recruitments")
            .leftJoinAndSelect("recruitments.expected_position","recruitmentExpectedPosition")
            .leftJoinAndSelect("recruitments.expected_project","recruitmentExpectedProject")
            .leftJoinAndSelect("recruitments.interviews","recruitmentInterviews")
            .leftJoinAndSelect("recruitmentInterviews.interviewers","recruitmentInterviewInterviewers")
            .leftJoinAndSelect("recruitmentInterviewInterviewers.person_id","recruitmentInterviewInterviewerPersons");

       return person.getOne();
    }

    async deletePerson(id) {
       let experiences = await Experiences.delete({person_id:id});
       let educations = await Educations.delete({person_id:id});
       let works = await Works.delete({person_id:id});
       let families = await Families.delete({person_id:id});
       let leaves = await Leaves.delete({registrant:id});
       let positions = await PersonPositions.delete({person_id:id});
       let images = await PersonImages.delete({person_id:id});
       let personSkills = await PersonSkills.delete({person_id:id});
       let socialNetworks = await PersonSocialNetworks.delete({person_id:id});
       let recruitments = await Recruitments.delete({person_id:id});
       return await Persons.delete({ id: id});
    }

    async fetchPositions() {
        let positions = await Positions.findAndCount();
        if(positions === undefined || positions[1] === 0){
            throw new HttpException({
                status: HttpStatus.NOT_FOUND,
                error: 'Data Not Found',
            }, 404);
        }
        return positions;
    }

    async addPersonPosition(personID,positionID){ // private function
        let personPosition = new PersonPositions();
        personPosition.person_id = personID;
        personPosition.position_id = positionID;
        return await personPosition.save();
    }

    async editPersonPosition(personID,positionID){ // private function
        let personPosition =  await PersonPositions.findOne({
            where: {
                person_id: personID
            }
        });
        personPosition.position_id = positionID;
        return await personPosition.save();
    }

    async skillsWizard(personID, data) { // private function
        await PersonSkills.delete({person_id:personID});
        if(data) {
            await data.forEach(async (skillsData) => {
                let skill = new PersonSkills();
                skill.person_id = personID;
                skill.skill_id = skillsData['skill_id'];
                await skill.save();
            });
        }
        return await Persons.findAndCount({where: { person_id: personID }});
    }
}
