import {getRepository, MigrationInterface, QueryRunner} from "typeorm";
import {DeductionTypes} from "../seeds/DeductionTypesSeed.seed";

export class SeedDeductionTypes1579598743749 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        const leaveTypes = await getRepository("deduction_types").save(DeductionTypes);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
