import { getRepository, MigrationInterface, QueryRunner } from 'typeorm';
import { PositionSeed } from '../seeds/PositionSeed.seed';

export class SeedPositions1579784063469 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    const positions = await getRepository('positions').save(PositionSeed);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {}
}
