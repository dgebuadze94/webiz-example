import {getRepository, MigrationInterface, QueryRunner} from "typeorm";
import { LanguageSeed } from "../seeds/LanguageSeed.seed";
export class SeedLanguages1579000328624 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        const languages = await getRepository("languages").save(LanguageSeed);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
