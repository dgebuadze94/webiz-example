import {getRepository, MigrationInterface, QueryRunner} from "typeorm";
import {LeaveTypes} from "../seeds/LeaveTypesSeed.seed";

export class SeedLeaveTypes1579519520657 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        const leaveTypes = await getRepository("leave_types").save(LeaveTypes);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
