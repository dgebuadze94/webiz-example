import 'dotenv/config';

import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import {Logger, ValidationPipe} from '@nestjs/common';

const port = process.env.PORT || 3000;

process.on('uncaughtException', function (err) {
  console.log("Error : Uncaught Exception");
  console.error(err);
});

async function bootstrap() {

  const app = await NestFactory.create(AppModule,{cors:true},);

  const options = new DocumentBuilder()
      .setTitle('HR Api Doc')
      .setDescription('HR Api Doc')
      .setVersion('1.0')
      .addTag('HR')
      .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api-doc', app, document);
  app.useGlobalPipes(
      new ValidationPipe({
        /*
              If set to true, instead of stripping non-whitelisted
              properties validator will throw an exception.
        */
        forbidNonWhitelisted: true,
        /*
              If set to true, validator will strip validated (returned)
              object of any properties that do not use any validation decorators.
        */
        whitelist: false,
        /*
      If set to true, validator will skip missing properties
*/
        skipMissingProperties:true
      }),
  );
  await app.listen(port);
  Logger.log(`Server running on PORT: ${port} `,'Bootstrap');


}
bootstrap();
