import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PersonController } from './modules/person/person.controller';
import { PersonModule } from './modules/person/person.module';
import {PersonService} from "./modules/person/person.service";
import { SkillsController } from './modules/skills/skills.controller';
import { SkillsService } from './modules/skills/skills.service';
import { SkillsModule } from './modules/skills/skills.module';
import { UploadModule } from './modules/upload/upload.module';
import { UploadService } from './modules/upload/upload.service';

@Module({
  imports: [TypeOrmModule.forRoot(),ConfigModule.forRoot({envFilePath:".development.env"}) , PersonModule, SkillsModule, ProjectModule, UploadModule,LeaveModule, RecruitmentModule, DeductionModule, CalculationModule, BonusModule, LanguageModule, ContractModule, WorkModule, FamilyModule, EducationModule, InterviewModule],
  controllers: [AppController, PersonController,SkillsController, ProjectController, LeaveController, RecruitmentController,InterviewController ],
  providers: [AppService, PersonService, ExperienceService, SkillsService, ProjectService, UploadService, LeaveService, RecruitmentService, CalculationService,InterviewService],
})
export class AppModule {}
