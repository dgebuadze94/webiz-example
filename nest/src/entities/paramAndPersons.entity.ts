import { Entity, Column, CreateDateColumn,UpdateDateColumn, PrimaryGeneratedColumn,BaseEntity } from 'typeorm';

@Entity()
export class ParamAndPersons extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: "bigint"})
    param_id: number;

    @Column({ type: "bigint"})
    person_id: number;

    @Column({ type: "varchar", length: 191, nullable: true })
    additional_value: string;

    @Column({ type: "boolean", default: true })
    show: Boolean;

    @CreateDateColumn()
    created_at: boolean;

    @UpdateDateColumn()
    updated_at: boolean;

}
