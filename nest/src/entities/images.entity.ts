import { Entity, Column, CreateDateColumn,UpdateDateColumn, PrimaryGeneratedColumn,BaseEntity } from 'typeorm';

@Entity()
export class Images extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: "varchar", length: 191})
    image: string;

    @Column({ type: "varchar", length: 191})
    image_thumbnail: string;

    @Column({ type: "varchar", length: 191, nullable: true})
    alt: string;

    @CreateDateColumn()
    created_at: boolean;

    @UpdateDateColumn()
    updated_at: boolean;

}
