import {
    Entity,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    PrimaryGeneratedColumn,
    OneToOne,
    OneToMany,
    JoinTable,
    JoinColumn,
    BaseEntity,
    ManyToOne,
    ManyToMany
} from 'typeorm';
import {Positions} from "./positions.entity";
import {Persons} from "./persons.entity";

@Entity()
export class Contracts extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => Persons,person => person.id , {nullable:true})
    @JoinColumn({name:"person_id"})
    person_id:Persons;

    @ManyToOne(type => Positions,position => position.id, {nullable:true})
    @JoinColumn({name:"position"})
    position:Positions;

    @Column( {type: "date", nullable: true})
    probation: Date;

    @Column({ type: "date", nullable: true})
    salary_review_date: Date;

    @Column({ type:"int", default: 0})
    salary_gross: number;

    @Column({ type: "date", nullable: true})
    start_date: Date;

    @Column({ type: "date", nullable: true})
    end_date: Date;

    @Column({ type: "date", nullable: true})
    termination_date: Date;


    @CreateDateColumn()
    created_at: boolean;

    @UpdateDateColumn()
    updated_at: boolean;



}
