import {
    Entity,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    PrimaryGeneratedColumn,
    OneToOne,
    OneToMany,
    JoinTable,
    JoinColumn,
    BaseEntity,
    ManyToOne,
    ManyToMany
} from 'typeorm';
import {Positions} from "./positions.entity";
import {Persons} from "./persons.entity";
import {RelationTypes} from "./relationTypes.entity";
import {DegreeTypes} from "./degreeTypes.entity";

@Entity()
export class Educations extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => Persons,person => person.id)
    @JoinColumn({name:"person_id"})
    person_id:Persons;

    @Column({type: "varchar",length:191})
    education_center:String;

    @Column({type: "varchar",length:191})
    specialization:String;

    @ManyToOne(type => DegreeTypes,degreeType => degreeType.id, {nullable:true})
    @JoinColumn({name:"degree"})
    degree:DegreeTypes;


    @Column({ type: "date", nullable: true})
    start_date: Date;

    @Column({ type: "date", nullable: true})
    end_date: Date;

    @Column({ type:"boolean", default: false})
    university_or_training: Boolean;

    @CreateDateColumn()
    created_at: boolean;

    @UpdateDateColumn()
    updated_at: boolean;



}
