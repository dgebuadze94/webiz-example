import { Entity, Column, CreateDateColumn,UpdateDateColumn, PrimaryGeneratedColumn,BaseEntity } from 'typeorm';

@Entity()
export class Departments extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: "bigint", default: 0 })
    parent_id: number;

    @Column({ type: "varchar", length: 191})
    department_name: string;

    @CreateDateColumn()
    created_at: boolean;

    @UpdateDateColumn()
    updated_at: boolean;

}
