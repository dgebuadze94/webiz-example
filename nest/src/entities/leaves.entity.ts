import {
    Entity,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    PrimaryGeneratedColumn,
    BaseEntity,
    OneToMany, JoinColumn, ManyToOne
} from 'typeorm';
import {LeaveTypes} from "./leaveTypes.entity";
import {Persons} from "./persons.entity";

@Entity()
export class Leaves extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => Persons,person => person.id)
    @JoinColumn({name:"person_id"})
    person_id:Persons;

    @Column({ type: "datetime"})
    start_date: Date;

    @Column({ type: "datetime"})
    end_date: Date;

    @ManyToOne(type => Persons,person => person.id)
    @JoinColumn({name:"registrant"})
    registrant:Persons;

    @ManyToOne(type => Persons,person => person.id)
    @JoinColumn({name:"approved_by"})
    approved_by:Persons;

    @Column({ type: "text", nullable: true})
    comment: string;

    @ManyToOne(type => LeaveTypes,leaveType => leaveType.id)
    @JoinColumn({name:"type_id"})
    type_id:LeaveTypes;

    @CreateDateColumn()
    created_at: boolean;

    @UpdateDateColumn()
    updated_at: boolean;

}
