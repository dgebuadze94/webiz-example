import {
    Entity,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    PrimaryGeneratedColumn,
    BaseEntity,
    OneToOne,ManyToOne, JoinColumn
} from 'typeorm';
import {CompanyDepartments} from "./companyDepartments.entity";
import {Persons} from "./persons.entity";
import {Positions} from "./positions.entity";
import {Skills} from "./skills.entity";
import {Deductions} from "./deductions.entity";

@Entity()
export class PersonDeductions extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => Persons,person => person.id)
    @JoinColumn({name:"person_id"})
    person_id:Persons;

    @ManyToOne(type => Deductions,deduction => deduction.id)
    @JoinColumn({name:"deduction_id"})
    deduction_id:Deductions;

    @Column({ type: "datetime"})
    start_date: Date;

    @Column({ type: "datetime"})
    termination_date: Date;

    @CreateDateColumn()
    created_at: boolean;

    @UpdateDateColumn()
    updated_at: boolean;

}
