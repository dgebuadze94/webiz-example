import { Entity, Column, CreateDateColumn,UpdateDateColumn, PrimaryGeneratedColumn, OneToMany, JoinColumn, BaseEntity } from 'typeorm';
import {Persons} from "./persons.entity";
import {CompanyDepartments} from "./companyDepartments.entity";

@Entity()
export class Companies extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: "varchar", length: 191})
    company_name: string;

    @CreateDateColumn()
    created_at: boolean;

    @UpdateDateColumn()
    updated_at: boolean;

}
