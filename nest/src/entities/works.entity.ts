import {
    Entity,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    PrimaryGeneratedColumn,
    OneToOne,
    OneToMany,
    JoinTable,
    JoinColumn,
    BaseEntity,
    ManyToOne,
    ManyToMany
} from 'typeorm';
import {Positions} from "./positions.entity";
import {Persons} from "./persons.entity";

@Entity()
export class Works extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => Persons,person => person.id)
    @JoinColumn({name:"person_id"})
    person_id:Persons;

    @Column({type: "varchar",length:191})
    company_name:String;

    @ManyToOne(type => Positions,position => position.id)
    @JoinColumn({name:"position"})
    position:Positions;

    @Column({ type:"text"})
    description: String;

    @Column({ type: "date", nullable: true})
    start_date: Date;

    @Column({ type: "date", nullable: true})
    end_date: Date;

    @CreateDateColumn()
    created_at: boolean;

    @UpdateDateColumn()
    updated_at: boolean;



}
