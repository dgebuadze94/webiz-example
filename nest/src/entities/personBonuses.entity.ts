import {
    Entity,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    PrimaryGeneratedColumn,
    BaseEntity,
    OneToOne,ManyToOne, JoinColumn
} from 'typeorm';
import {Persons} from "./persons.entity";
import {Bonuses} from "./bonuses.entity";

@Entity()
export class PersonBonuses extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => Persons,person => person.id)
    @JoinColumn({name:"person_id"})
    person_id:Persons;

    @ManyToOne(type => Bonuses,bonuse => bonuse.id)
    @JoinColumn({name:"bonuse_id"})
    bonuse_id:Bonuses;

    @Column({ type: "datetime"})
    start_date: Date;

    @Column({ type: "datetime"})
    termination_date: Date;

    @CreateDateColumn()
    created_at: boolean;

    @UpdateDateColumn()
    updated_at: boolean;

}
