import {
    Entity,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    PrimaryGeneratedColumn,
    BaseEntity,
    ManyToOne, JoinColumn
} from 'typeorm';
import {Persons} from "./persons.entity";
import {Images} from "./images.entity";

@Entity()
export class PersonImages extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => Persons,person => person.id)
    @JoinColumn({name:"person_id"})
    person_id:Persons;

    @ManyToOne(type => Images,image => image.id)
    @JoinColumn({name:"image_id"})
    image_id:Images;

    @CreateDateColumn()
    created_at: boolean;

    @UpdateDateColumn()
    updated_at: boolean;

}
