import { Entity, Column, CreateDateColumn,UpdateDateColumn, PrimaryGeneratedColumn,BaseEntity } from 'typeorm';

@Entity()
export class LeaveTypes extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: "varchar", length: 191})
    name: string;

    @Column({ type: "int", nullable: true})
    paid_days: number;

    @Column({ type: "int", nullable: true})
    unpaid_days: number;

    @CreateDateColumn()
    created_at: boolean;

    @UpdateDateColumn()
    updated_at: boolean;

}
