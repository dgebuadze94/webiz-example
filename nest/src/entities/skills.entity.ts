import {
    Entity,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    PrimaryGeneratedColumn,
    BaseEntity,
    ManyToOne, JoinColumn
} from 'typeorm';
import {LeaveTypes} from "./leaveTypes.entity";
import {SkillTypes} from "./skillTypes.entity";
import {SkillLevels} from "./skillLevels.entity";

@Entity()
export class Skills extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: "varchar", length: 191, unique:true})
    name: string;

    @ManyToOne(type => SkillTypes,skillType => skillType.id)
    @JoinColumn({name:"type_id"})
    type_id:SkillTypes;

    @CreateDateColumn()
    created_at: boolean;

    @UpdateDateColumn()
    updated_at: boolean;

}
