import {
    Entity,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    PrimaryGeneratedColumn,
    BaseEntity,
    OneToOne,ManyToOne, JoinColumn
} from 'typeorm';
import {CompanyDepartments} from "./companyDepartments.entity";
import {Persons} from "./persons.entity";
import {Positions} from "./positions.entity";

@Entity()
export class PersonPositions extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => Persons,person => person.id)
    @JoinColumn({name:"person_id"})
    person_id:Persons;

    @ManyToOne(type => Positions,positions => positions.id)
    @JoinColumn({name:"position_id"})
    position_id:Positions;

    @CreateDateColumn()
    created_at: boolean;

    @UpdateDateColumn()
    updated_at: boolean;

}
