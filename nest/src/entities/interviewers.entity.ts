import {
    Entity,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    PrimaryGeneratedColumn,
    BaseEntity,
    OneToOne,ManyToOne, JoinColumn
} from 'typeorm';
import {CompanyDepartments} from "./companyDepartments.entity";
import {Persons} from "./persons.entity";
import {Positions} from "./positions.entity";
import {Interviews} from "./interviews.entity";

@Entity()
export class Interviewers extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => Persons,person => person.id)
    @JoinColumn({name:"person_id"})
    person_id:Persons;

    @ManyToOne(type => Interviews,interview => interview.id)
    @JoinColumn({name:"interview_id"})
    interview_id:Interviews;

    @CreateDateColumn()
    created_at: boolean;

    @UpdateDateColumn()
    updated_at: boolean;

}
