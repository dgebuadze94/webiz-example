import {
  Entity,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
  BaseEntity,
  OneToMany,
  JoinColumn,
  ManyToOne,
} from 'typeorm';
import { Positions } from './positions.entity';
import { Persons } from './persons.entity';
import { Projects } from './projects.entity';
import {Interviews} from "./interviews.entity";

@Entity()
export class Recruitments extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(
    type => Persons,
    person => person.id,
  )
  @JoinColumn({ name: 'person_id' })
  person_id: Persons;

  @Column({ type: 'date' })
  start_date: Date;

  @Column({ type: 'varchar', length: 191 })
  current_job: string;

  @Column({ type: 'int' })
  current_salary: number;

  @Column({ type: 'int' })
  expected_salary: number;

  @ManyToOne(type => Positions, position => position.id)
  @JoinColumn({ name: 'expected_position' })
  expected_position: Positions;

  @ManyToOne(type => Projects, project => project.id)
  @JoinColumn({ name: 'expected_project' })
  expected_project: Projects;

  @OneToMany(type => Interviews, interview => interview.recruitment_id,{nullable:true})
  @JoinColumn({ name: 'interviews'})
  interviews: Interviews;

  @CreateDateColumn()
  created_at: boolean;

  @UpdateDateColumn()
  updated_at: boolean;
}
