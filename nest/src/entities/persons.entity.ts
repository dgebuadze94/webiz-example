import {
    Entity,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    PrimaryGeneratedColumn,
    OneToOne,
    OneToMany,
    JoinTable,
    JoinColumn,
    BaseEntity,
    ManyToOne,
    ManyToMany
} from 'typeorm';
import {CompanyDepartments} from "./companyDepartments.entity";
import {PersonPositions} from "./personPositions.entity";
import {PersonLanguages} from "./personLanguages.entity";
import {Experiences} from "./experiences.entity";
import {PersonProjects} from "./personProjects.entity";
import {PersonSkills} from "./personSkills.entity";
import {PersonDeductions} from "./personDeductions.entity";
import { Recruitments } from './recruitments.entity';
import {PersonBonuses} from "./personBonuses.entity";
import {Contracts} from "./contracts.entity";
import {Works} from "./works.entity";
import {Educations} from "./educations.entity";
import {Families} from "./families.entity";
import {Leaves} from "./leaves.entity";

@Entity()
export class Persons extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: "varchar", length: 191})
    first_name: string;

    @Column({ type: "varchar", length: 191})
    last_name: string;

    @Column({ type: "date", nullable: true })
    date_of_birth: Date;

    @Column({ type: "varchar", length: 191,nullable: true })
    phone: string;

    @Column({ type: "varchar", length: 191, unique: true, nullable: true })
    email: string;

    @Column({ type: "varchar", length: 191, nullable: true})
    address: string;

    @ManyToOne(type => CompanyDepartments,companyDepartments => companyDepartments.id,{nullable:true})
    @JoinColumn({name:"company_department_id"})
    company_department_id:CompanyDepartments;

    @Column({ type: "text", nullable: true })
    bio: string;

    @Column({ type: "varchar", length: 191, nullable: true})
    cv: string;

    @OneToMany(type => Families,family => family.person_id,{nullable:true})
    @JoinColumn({name:"families"})
    families:Families;

    @OneToMany(type => Works,work => work.person_id,{nullable:true})
    @JoinColumn({name:"works"})
    works:Works;

    @OneToMany(type => Educations,education => education.person_id,{nullable:true})
    @JoinColumn({name:"works"})
    educations:Educations;

    @OneToMany(type => Contracts,contract => contract.person_id,{nullable:true})
    @JoinColumn({name:"contracts"})
    contracts:Contracts;

    @OneToMany(type => PersonPositions,personPositions => personPositions.person_id,{nullable:true})
    @JoinColumn({name:"positions"})
    positions:PersonPositions;

    @OneToMany(type => PersonLanguages,personLanguages => personLanguages.person_id,{nullable:true})
    @JoinColumn({name:"languages"})
    languages:PersonLanguages;

    @OneToMany(type => Experiences,experience => experience.person_id,{nullable:true})
    @JoinColumn({name:"experience"})
    experience:Experiences;

    @OneToMany(type => PersonProjects,personProject => personProject.person_id,{nullable:true})
    @JoinColumn({name:"projects"})
    projects:PersonProjects;

    @OneToMany(type => PersonSkills,personSkill => personSkill.person_id,{nullable:true})
    @JoinColumn({name:"skills"})
    skills:PersonSkills;

    @OneToMany(type => PersonDeductions,personDeduction => personDeduction.person_id,{nullable:true})
    @JoinColumn({name:"deductions"})
    deductions:PersonDeductions;

    @OneToMany(type => PersonBonuses,personBonuse => personBonuse.person_id,{nullable:true})
    @JoinColumn({name:"bonuses"})
    bonuses:PersonBonuses;

    
    @OneToMany(type => Recruitments, recruitment => recruitment.person_id,{nullable:true})
    @JoinColumn({ name: 'recruitments'})
    recruitments: Recruitments;

    @OneToMany(type => Leaves, leave => leave.person_id,{nullable:true})
    @JoinColumn({ name: 'leaves'})
    leaves: Leaves;

    @CreateDateColumn()
    created_at: boolean;

    @UpdateDateColumn()
    updated_at: boolean;



}
