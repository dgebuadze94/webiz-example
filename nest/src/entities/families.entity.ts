import {
    Entity,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    PrimaryGeneratedColumn,
    OneToOne,
    OneToMany,
    JoinTable,
    JoinColumn,
    BaseEntity,
    ManyToOne,
    ManyToMany
} from 'typeorm';
import {Positions} from "./positions.entity";
import {Persons} from "./persons.entity";
import {RelationTypes} from "./relationTypes.entity";

@Entity()
export class Families extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => Persons,person => person.id)
    @JoinColumn({name:"person_id"})
    person_id:Persons;

    @Column({type: "varchar",length:191})
    full_name:String;

    @Column({type: "varchar",length:30})
    phone:String;

    @ManyToOne(type => RelationTypes,relationType => relationType.id)
    @JoinColumn({name:"position"})
    relation_type:RelationTypes;

    @Column({ type:"boolean", default: false})
    is_emergency_contact: Boolean;

    @CreateDateColumn()
    created_at: boolean;

    @UpdateDateColumn()
    updated_at: boolean;



}
