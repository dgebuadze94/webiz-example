import {
    Entity,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    PrimaryGeneratedColumn,
    BaseEntity,
    OneToMany, JoinColumn, ManyToOne
} from 'typeorm';
import {LeaveTypes} from "./leaveTypes.entity";
import {Persons} from "./persons.entity";
import {DeductionTypes} from "./deductionTypes.entity";
import {PersonDeductions} from "./personDeductions.entity";

@Entity()
export class Deductions extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: "int",  default: 0})
    amount_from_salary: number;

    @Column({ type: "int", default: 0})
    amount_from_company: number;

    @Column({ type: "varchar",length:191})
    name: string;

    @Column({ type: "text"})
    description: string;

    @Column({ type: "boolean", default: false})
    is_custom: Boolean;

    @ManyToOne(type => DeductionTypes,deductionType => deductionType.id)
    @JoinColumn({name:"type_id"})
    type_id:DeductionTypes;

    @OneToMany(type => PersonDeductions,personDeduction => personDeduction.deduction_id)
    @JoinColumn({name:"persons"})
    persons:PersonDeductions;

    @CreateDateColumn()
    created_at: boolean;

    @UpdateDateColumn()
    updated_at: boolean;

}
