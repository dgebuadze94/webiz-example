import {
    Entity,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    PrimaryGeneratedColumn,
    BaseEntity,
    OneToOne,ManyToOne, JoinColumn
} from 'typeorm';
import {Persons} from "./persons.entity";
import {Projects} from "./projects.entity";

@Entity()
export class PersonProjects extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => Persons,person => person.id)
    @JoinColumn({name:"person_id"})
    person_id:Persons;

    @ManyToOne(type => Projects,project => project.id)
    @JoinColumn({name:"project_id"})
    project_id:Projects;

    @CreateDateColumn()
    created_at: boolean;

    @UpdateDateColumn()
    updated_at: boolean;

}
