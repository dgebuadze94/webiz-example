import {
    Entity,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    PrimaryGeneratedColumn,
    BaseEntity,
    OneToOne,ManyToOne, JoinColumn
} from 'typeorm';
import {CompanyDepartments} from "./companyDepartments.entity";
import {Persons} from "./persons.entity";
import {Positions} from "./positions.entity";
import {Skills} from "./skills.entity";
import {SkillLevels} from "./skillLevels.entity";

@Entity()
export class PersonSkills extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => Persons,person => person.id)
    @JoinColumn({name:"person_id"})
    person_id:Persons;

    @ManyToOne(type => Skills,skill => skill.id)
    @JoinColumn({name:"skill_id"})
    skill_id:Skills;

    @Column({ type: "date", nullable: true})
    valid_from: Date;

    @ManyToOne(type => Persons,person => person.id)
    @JoinColumn({name:"issued_by"})
    issued_by:Persons;

    @ManyToOne(type => SkillLevels,skillLevel => skillLevel.id)
    @JoinColumn({name:"level_id"})
    level_id:SkillLevels;

    @CreateDateColumn()
    created_at: boolean;

    @UpdateDateColumn()
    updated_at: boolean;

}
