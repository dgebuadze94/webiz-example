import { Entity, Column, CreateDateColumn,UpdateDateColumn, PrimaryGeneratedColumn,BaseEntity } from 'typeorm';

@Entity()
export class Languages extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: "int", unique: true})
    lang_id: number;

    @Column({ type: "varchar", length: 20, nullable: true})
    language: string;

    @Column({ type: "varchar", length: 20, nullable: true})
    lang_acronym: string;

    @Column({ type: "int", nullable: true})
    order_id: number;

    @CreateDateColumn()
    created_at: boolean;

    @UpdateDateColumn()
    updated_at: boolean;
}
