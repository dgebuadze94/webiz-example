import {
    Entity,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    PrimaryGeneratedColumn,
    BaseEntity,
    ManyToOne, JoinColumn
} from 'typeorm';
import {Persons} from "./persons.entity";
import {SocialNetworks} from "./socialNetworks.entity";

@Entity()
export class PersonSocialNetworks extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => Persons,person => person.id)
    @JoinColumn({name:"person_id"})
    person_id:Persons;

    @ManyToOne(type => SocialNetworks,socialNetwork => socialNetwork.id)
    @JoinColumn({name:"social_network_id"})
    social_network_id:SocialNetworks;

    @CreateDateColumn()
    created_at: boolean;

    @UpdateDateColumn()
    updated_at: boolean;

}
