import {
    Entity,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    PrimaryGeneratedColumn,
    OneToOne,
    JoinColumn,
    BaseEntity,
    ManyToOne
} from 'typeorm';
import {Persons} from "./persons.entity";
import {Companies} from "./companies.entity";
import {Departments} from "./departments.entity";

@Entity()
export class CompanyDepartments extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => Companies,companies => companies.id)
    @JoinColumn({name:"company_id"})
    company_id:Companies;

    @ManyToOne(type => Departments,departments => departments.id)
    @JoinColumn({name:"department_id"})
    department_id:Departments;

    @CreateDateColumn()
    created_at: boolean;

    @UpdateDateColumn()
    updated_at: boolean;
}
