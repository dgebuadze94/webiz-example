import { Entity, Column, CreateDateColumn,UpdateDateColumn, PrimaryGeneratedColumn,BaseEntity } from 'typeorm';

@Entity()
export class Params extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: "bigint"})
    type_id: string;

    @Column({ type: "bigint", default: 0 })
    parent_id: number;

    @Column({ type: "varchar", length: 191, nullable: true })
    name: string;

    @CreateDateColumn()
    created_at: boolean;

    @UpdateDateColumn()
    updated_at: boolean;

}
