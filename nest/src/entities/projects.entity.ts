import {
    Entity,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    PrimaryGeneratedColumn,
    BaseEntity,
    OneToMany, JoinColumn
} from 'typeorm';
import {PersonProjects} from "./personProjects.entity";

@Entity()
export class Projects extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: "varchar", length: 191})
    project: string;

    @Column({ type: "date"})
    start_date: Date;

    @Column({ type: "int"})
    status: number;

    @Column({ type: "date", nullable:true})
    done_date: Date;

    @OneToMany(type => PersonProjects,personProject => personProject.project_id)
    @JoinColumn({name:"persons"})
    persons:PersonProjects;

    @CreateDateColumn()
    created_at: boolean;

    @UpdateDateColumn()
    updated_at: boolean;

}
