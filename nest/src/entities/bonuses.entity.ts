import {
    Entity,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    PrimaryGeneratedColumn,
    BaseEntity,
    OneToMany, JoinColumn, ManyToOne
} from 'typeorm';
import {LeaveTypes} from "./leaveTypes.entity";
import {Persons} from "./persons.entity";
import {DeductionTypes} from "./deductionTypes.entity";
import {PersonBonuses} from "./personBonuses.entity";

@Entity()
export class Bonuses extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: "int", default:0})
    amount_from_salary: number;

    @Column({ type: "int", default: 0})
    amount_from_company: number;

    @Column({ type: "varchar",length:191})
    name: string;

    @Column({ type: "text"})
    description: string;

    @Column({ type: "boolean", default: false})
    is_custom: Boolean;

    @OneToMany(type => PersonBonuses,personBonus => personBonus.bonuse_id)
    @JoinColumn({name:"persons"})
    persons:PersonBonuses;

    @CreateDateColumn()
    created_at: boolean;

    @UpdateDateColumn()
    updated_at: boolean;

}
