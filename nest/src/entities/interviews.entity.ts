import {
  Entity,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
  BaseEntity,
  OneToMany,
  JoinColumn,
  ManyToOne,
} from 'typeorm';
import {Interviewers} from "./interviewers.entity";
import {Recruitments} from "./recruitments.entity";

@Entity()
export class Interviews extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(type => Recruitments,recruitment => recruitment.id)
  @JoinColumn({name:"recruitment_id"})
  recruitment_id: Recruitments;

  @Column({ type: 'date' })
  interview_date: Date;

  @OneToMany(type => Interviewers, interviewer => interviewer.interview_id,{nullable:true})
  @JoinColumn({ name: 'interviewers'})
  interviewers: Interviewers;

  @Column({ type: 'boolean', default: false })
  completed: Boolean;

  @CreateDateColumn()
  created_at: boolean;

  @UpdateDateColumn()
  updated_at: boolean;
}
