import {
    Entity,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    PrimaryGeneratedColumn,
    BaseEntity,
    ManyToOne, JoinColumn
} from 'typeorm';
import {Persons} from "./persons.entity";

@Entity()
export class Experiences extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => Persons,person => person.id)
    @JoinColumn({name:"person_id"})
    person_id:Persons;

    @Column({ type: "varchar", length: 255})
    title: string;

    @Column({ type: "varchar", length: 255})
    organization: string;

    @Column({ type: "date", nullable: true })
    start_date: Date;

    @Column({ type: "date", nullable: true })
    end_date: Date;

    @Column({ type: "text", nullable: true})
    description: string;

    @Column({ type: "set", enum: ["experience", "education"], default: ["education"]})
    type: "experience" | "education";

    @CreateDateColumn()
    created_at: boolean;

    @UpdateDateColumn()
    updated_at: boolean;

}
