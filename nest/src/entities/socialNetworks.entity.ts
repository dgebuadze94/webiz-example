import { Entity, Column, CreateDateColumn,UpdateDateColumn, PrimaryGeneratedColumn,BaseEntity } from 'typeorm';

@Entity()
export class SocialNetworks extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: "bigint", nullable: true})
    socialNetworkImageID: number;

    @Column({ type: "varchar", length: 191})
    socialNetwork: string;

    @Column({ type: "varchar", length: 191})
    socialNetworkUrl: string;

    @Column({ type: "text", nullable: true})
    description: string;

    @CreateDateColumn()
    createdAt: boolean;

    @UpdateDateColumn()
    updatedAt: boolean;

}
