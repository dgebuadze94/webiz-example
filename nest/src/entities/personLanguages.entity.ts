import {
    Entity,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    PrimaryGeneratedColumn,
    BaseEntity,
    OneToOne,ManyToOne, JoinColumn
} from 'typeorm';
import {Persons} from "./persons.entity";
import {Languages} from "./languages.entity";

@Entity()
export class PersonLanguages extends BaseEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => Persons,person => person.id)
    @JoinColumn({name:"person_id"})
    person_id:Persons;

    @ManyToOne(type => Languages,language => language.id)
    @JoinColumn({name:"language_id"})
    language_id:Languages;

    @CreateDateColumn()
    created_at: boolean;

    @UpdateDateColumn()
    updated_at: boolean;

}
