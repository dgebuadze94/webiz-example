export const LeaveTypes = [
  {
    name:"Sick",
    created_at: `${new Date()}`,
    updated_at: `${new Date()}`
  },
  {
    name:"University",
    created_at: `${new Date()}`,
    updated_at: `${new Date()}`
  },
  {
    name:"Vacation",
    created_at: `${new Date()}`,
    updated_at: `${new Date()}`
  },
  {
    name:"Day off",
    created_at: `${new Date()}`,
    updated_at: `${new Date()}`
  },

];
