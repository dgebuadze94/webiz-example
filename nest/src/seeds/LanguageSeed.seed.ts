export const LanguageSeed = [
  {
    lang_id: 1,
    language:"English",
    lang_acronym:"EN",
    order_id:1,
    created_at: `${new Date()}`,
    updated_at: `${new Date()}`
  },
  {
  lang_id: 2,
  language:"Georgia",
  lang_acronym:"KA",
  order_id:2,
  created_at: `${new Date()}`,
  updated_at: `${new Date()}`
  },

];
